# Genamoeba (submission to Ludum Dare 46)

https://ldjam.gitlab.io/46/genamoeba/

## Initial Setup

After cloning the repo.

_Windows Users_: Developers in a Windows environment may need to install build tools to compile native modules:
```bash
npm install -g windows-build-tools
```

Install all top-level packages (these are packages required to manage the monorepo):
```bash
npm install
```

Then just run `start`!
```bash
npm run start
```

You can also run `dev` on any `.html` file.
```bash
npm run dev ./src/app-sandbox-pixi/index.html
```

## Top-Level Operations

All of the following operations may be performed at the root (the folder containing lerna.json).

### Test

NOTE: I don't expect to see many tests...

```bash
npm run test
npm run test:coverage  # Also emit a coverage report
```
This runs Jest on all `*.test.ts` files and (optionally) emits a coverage report in the `coverage` folder.
