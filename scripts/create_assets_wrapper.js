const fs = require("fs");
const path = require("path");

const EXTENSIONS = ["png"];

main(process.argv);

const nl = `
`;

function printUsage() {
  console.log("node create_assets_wrapper.js <filePath> <targetDir>");
}

function die(msg) {
  console.log(msg);
  console.log("");
  process.exit(1);
}

function getCommentSection() {
  return `/**
/* DO NOT CHANGE!
/*
/* This file is auto generated by running 
/* npm run build:assets:amoebas
 */`;
}

function getImportLine(targetDir, filePath) {
  const name = path.parse(filePath).name;
  return `import ${name} from "./${targetDir}/${filePath}";`;
}

function getObjectSection(fileNames) {
  return `export const LOOKUP = {
${fileNames.map((x) => `  "${x}": ${x},`).join(nl)}
} as Record<string, string>;`;
}

function getExportSection(fileNames) {
  return `export {
${fileNames.map((x) => `  ${x},`).join(nl)}
};`;
}

function getContent(targetDir, filePaths) {
  const importSection = filePaths
    .map((x) => getImportLine(targetDir, x))
    .join(nl);

  const fileNames = filePaths.map((x) => path.parse(x).name);

  return `${getCommentSection()}
${importSection}

${getObjectSection(fileNames)}

${getExportSection(fileNames)}
`;
}

function main([, , filePath, targetDirPath]) {
  if (!filePath || !targetDirPath) {
    console.log("Please provide a filePath and a targetDirectory");
    printUsage();
    process.exit(1);
    return;
  }

  const targetDir = path.basename(targetDirPath);

  fs.promises.readdir(targetDirPath).then((files) => {
    const assetFiles = files.filter((x) =>
      EXTENSIONS.some((ext) => x.endsWith(ext))
    );
    const content = getContent(targetDir, assetFiles);
    fs.writeFileSync(filePath, content);
  });
}
