import * as PIXI from "pixi.js";
import {ECS, EntityStore} from "../ecs";
import { Viewport } from "pixi-viewport";
import { TransformComponent } from "../game/components/transform";
import { SpriteComponent } from "../game/components/sprite";
import { SpriteRenderingSystem } from "../game/systems/SpriteRenderingSystem";
import { CameraComponent, MovementComponent } from "../game/components";
import { CameraSystem } from "../game/systems/CameraSystem";
import { FollowTargetSystem } from "../game/systems/FollowTargetSystem";
import { MovementSystem } from "../game/systems/MovementSystem";
import { FollowTargetComponent } from "../game/components/followTarget";

function main(mainEl: Element)
{
    const app = new PIXI.Application({
        width: 800,
        height: 600,
        backgroundColor: 0x1099bb,
        resolution: window.devicePixelRatio || 1
    })
    mainEl.appendChild(app.view);

    var entityStore = new EntityStore();

    var spriteEntity1 = 
        entityStore
            .createEntity()
            .addComponents(
                new TransformComponent([0, 0]),
                new MovementComponent(0, [1, 1]),
                new SpriteComponent(
                    [140, 100],
                    [0.5, 0.5],
                    0xff0000
                )
            );

    var spriteEntity2 = 
        entityStore
            .createEntity()
            .addComponents(
                new TransformComponent([-200, -300]),
                new MovementComponent(0, [0, 0]),
                new SpriteComponent(
                    [140, 100],
                    [0.5, 0.5],
                    0xff0000
                ),
                new FollowTargetComponent(spriteEntity1.id)
            );

    var cameraEntity =
        entityStore
            .createEntity()
            .addComponents(
                new TransformComponent([0, 0]),
                new CameraComponent(800, 600),
                new MovementComponent(1),
                new FollowTargetComponent(spriteEntity2.id)
            );

    const ecs = 
        new ECS(entityStore)
            .addSystem(new FollowTargetSystem(entityStore))
            .addSystem(new MovementSystem())
            .addSystem(new SpriteRenderingSystem())
            .addSystem(new CameraSystem(app.stage));

    app.ticker.add((delta) =>
    {
        ecs.updateSystems(delta);
    });
}

document.addEventListener('DOMContentLoaded', () => {
    const el = document.getElementById('main');

    if (!el) {
        throw new Error('main element not found');
    }

    main(el);
});
