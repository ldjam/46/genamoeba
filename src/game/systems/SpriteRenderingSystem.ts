﻿import * as PIXI from "pixi.js";
import { System, Entity } from "../../ecs";
import { SpriteComponent } from "../components/sprite";
import { TransformComponent } from "../components/transform";
import { CameraComponent } from "../components";

export class SpriteRenderingSystem extends System<"cameras" | "entities">
{     
    public query = { 
        cameras: [ CameraComponent ],
        entities: [ SpriteComponent, TransformComponent ]
    };
    public run = (
        dt: number, 
        { entities, cameras } : Record<string, Entity[]>,
    ) =>
    {
        cameras.forEach(cameraEntity => {
            entities.forEach(entity => {
                const spriteInfo = entity.getComponent<SpriteComponent>(SpriteComponent);
                const transform = entity.getComponent<TransformComponent>(TransformComponent);
                
                if (!spriteInfo || !transform)
                {
                    throw new Error("Something went wrong");
                }

                spriteInfo.sprite.setTransform(
                    transform.position[0],
                    transform.position[1],
                    transform.scale[0],
                    transform.scale[1],
                    transform.rotation,
                    0,
                    0,
                    0,
                    0
                );

                spriteInfo.sprite.width = spriteInfo.dimensions[0];
                spriteInfo.sprite.height = spriteInfo.dimensions[1];

                spriteInfo.sprite.tint = spriteInfo.tint;
                spriteInfo.sprite.anchor.set(spriteInfo.anchor[0], spriteInfo.anchor[1]);
                spriteInfo.sprite.zIndex = transform.position[2];

                const camera = cameraEntity.getComponent<CameraComponent>(CameraComponent);
                camera?.viewport.addChild(spriteInfo.sprite);
            });
        });
    }
}
