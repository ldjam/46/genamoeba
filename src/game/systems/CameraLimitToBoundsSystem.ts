import { System, Entity } from "../../ecs";
import { CameraComponent, TransformComponent } from "../components";
import { CameraBoundsComponent } from "../components/CameraBounds";
import { CameraHitBoundsThisFrame } from "../components/CameraHitBoundsThisFrame";

export class CameraLimitToBoundsSystem extends System<"entities"> 
{
    constructor() {
        super();
    }

    public query = {
        entities: [CameraComponent, TransformComponent, CameraBoundsComponent]
    };

    public run = (
        dt: number, 
        { entities }: Record<string, Entity[]>
    ) => {
        entities.forEach(entity => {
            entity.removeComponents(CameraHitBoundsThisFrame);

            const camera = entity.getComponent<CameraComponent>(CameraComponent);
            const transform = entity.getComponent<TransformComponent>(TransformComponent);
            const bounds = entity.getComponent<CameraBoundsComponent>(CameraBoundsComponent);

            if (!camera || !transform || !bounds) 
            {
                return;
            }

            let hitBounds = false;
            const viewportBounds = camera.viewport.getVisibleBounds();

            if (viewportBounds.y + viewportBounds.height > bounds.y + bounds.height) {
                var limit = transform.position[1] + ((bounds.y + bounds.height) - (viewportBounds.y + viewportBounds.height));
                transform.position[1] = limit;
                hitBounds = true;
            }

            if (viewportBounds.x < bounds.x) {
                var limit = transform.position[0] + (bounds.x - viewportBounds.x);
                transform.position[0] = limit;
                hitBounds = true;
            }

            if (viewportBounds.x + viewportBounds.width > bounds.x + bounds.width) {
                var limit = transform.position[0] + ((bounds.x + bounds.width) - (viewportBounds.x + viewportBounds.width));
                transform.position[0] = limit;
                hitBounds = true;
            }

            if (viewportBounds.y < bounds.y) {
                var limit = transform.position[1] + (bounds.y - viewportBounds.y);
                transform.position[1] = limit;
                hitBounds = true;
            }

            if (hitBounds) 
            {
                //camera.viewport.moveCenter(transform.position[0], transform.position[1]);
                entity.addComponents(new CameraHitBoundsThisFrame());
            }
        });
    };
}
