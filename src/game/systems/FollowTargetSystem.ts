import { System, Entity, EntityStore } from "../../ecs";
import { TransformComponent, MovementComponent, MoveToComponent } from "../components";
import { FollowTargetComponent } from "../components/followTarget";

type EntityQuery = "entities";
export class FollowTargetSystem extends System<EntityQuery>
{    
    constructor(
        private entityStore: EntityStore
    )
    {
        super();
    }

    public query = { entities: [ MovementComponent, TransformComponent, FollowTargetComponent ] };
    public run = (
        dt: number, 
        { entities }: Record<EntityQuery, Entity[]>
    ) =>
    {
        entities.forEach(
            entity =>
            {
                var followTargetComponent = entity.getComponent<FollowTargetComponent>(FollowTargetComponent);
                var movementComponent = entity.getComponent<MovementComponent>(MovementComponent);
                var transformComponent = entity.getComponent<TransformComponent>(TransformComponent);

                if (!followTargetComponent || !movementComponent || !transformComponent)
                {
                    throw new Error("required components missing");
                }

                if (!followTargetComponent.target)
                {
                    entity.removeComponents(FollowTargetComponent);
                    return;
                }

                var followEntity = this.entityStore.findEntity(followTargetComponent.target);

                if (!followEntity)
                {
                    entity.removeComponents(FollowTargetComponent);
                    return;
                }

                var followTransform = followEntity.getComponent<TransformComponent>(TransformComponent);

                if (!followTransform)
                {
                    entity.removeComponents(FollowTargetComponent);
                    return;
                }

                if (!followTransform.position)
                {
                    entity
                        .addComponents(
                            new MoveToComponent(followTransform.position)
                        );
                }
            }
        )
    }
}
