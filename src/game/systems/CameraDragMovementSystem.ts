import * as PIXI from "pixi.js";
import { System, Entity } from "../../ecs";
import { CameraComponent, TransformComponent } from "../components";

export class CameraDragMovementSystem extends System<"entities">
{
    private isDragging: boolean = false;
    private lastPos: [number, number] = [0, 0];
    private curPos: [number, number] = [0, 0];

    constructor(
        container: PIXI.Container
    )
    {
        super();

        container.interactive = true;

        container
            .on("pointerdown", this.onDragStart)
            .on("pointerup", this.onDragEnd)
            .on("pointerupoutside", this.onDragEnd)
            .on("pointermove", this.onDragMove);
    }

    public query = { entities: [ CameraComponent, TransformComponent ] };
    public run = (
        dt: number,
        { entities }: Record<string, Entity[]>
    ) => 
    {
        if (!this.isDragging)
        {
            return;
        }

        const delta = [
            this.curPos[0] - this.lastPos[0],
            this.curPos[1] - this.lastPos[1]
        ];

        entities.forEach(
            entity =>
            {
                const transform = entity.getComponent<TransformComponent>(TransformComponent);
                const cameraComponent = entity.getComponent<CameraComponent>(CameraComponent);

                if (!transform || !cameraComponent)
                {
                    return;
                }

                transform.position[0] -= delta[0] * 1;
                transform.position[1] -= delta[1] * 1;
            }
        );

        this.lastPos = this.curPos;
    }

    private onDragStart = (ev: PIXI.interaction.InteractionEvent) =>
    {
        this.isDragging = true;
        this.lastPos = this.curPos = [ev.data.global.x, ev.data.global.y];
    }

    private onDragEnd = (ev: PIXI.interaction.InteractionEvent) =>
    {
        this.isDragging = false;
        this.lastPos = this.curPos = [0, 0];
    }

    private onDragMove = (ev: PIXI.interaction.InteractionEvent) =>
    {
        this.curPos = [
            ev.data.global.x,
            ev.data.global.y
        ];
    }
}
