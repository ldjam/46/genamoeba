import { System, Entity } from "../../ecs";
import { CameraComponent, TransformComponent, MovementComponent } from "../components";
import { CameraUseEdgeMovementBehavior } from "../components/CameraUseEdgeMovementBehavior";
import { vec2 } from "gl-matrix";

type Aspect = "cameras";
export class CameraEdgeMovementSystem extends System<Aspect>
{
    constructor(
        private app: PIXI.Application
    )
    {
        super();
    }

    public query = {
        cameras: [ CameraComponent, TransformComponent, CameraUseEdgeMovementBehavior, MovementComponent ]
    };

    public run = (
        dt: number,
        { cameras } : Record<Aspect, Entity[]>
    ) =>
    {
        cameras.forEach(
            camera =>
            {
                const cameraComponent = camera.getComponent<CameraComponent>(CameraComponent);
                const cameraTransform = camera.getComponent<TransformComponent>(TransformComponent);
                const behavior = camera.getComponent<CameraUseEdgeMovementBehavior>(CameraUseEdgeMovementBehavior);
                const cameraMovement = camera.getComponent<MovementComponent>(MovementComponent);

                if (!cameraComponent || !cameraTransform || !behavior || !cameraMovement)
                {
                    return;
                }

                var {x, y} = this.app.renderer.plugins.interaction.mouse.global;
                
                let horizontal = 0;
                let vertical = 0;

                if (behavior.left && x < behavior.left)
                {
                    horizontal = -1;
                }
                else if (behavior.right && x > window.innerWidth - behavior.right)
                {
                    horizontal = 1;
                }

                if (behavior.top && y < behavior.top)
                {
                    vertical = -1;
                }
                else if (behavior.bottom && y > window.innerHeight - behavior.bottom)
                {
                    vertical = 1;
                }
                
                cameraMovement.direction = [horizontal, vertical];

                // const radiusSquared = Math.pow(behavior.radius, 2);
                // if (radiusSquared)
                // {
                //     const center = cameraComponent.viewport.toScreen(cameraComponent.viewport.center);
                //     const distance = Math.pow(center.x - x, 2) + Math.pow(center.y - y, 2);
                //     if (distance >= radiusSquared)
                //     {
                //         const angle = Math.atan2(center.y - y, center.x - x);
                //         cameraMovement.direction = [
                //             -Math.round(Math.cos(angle)),
                //             -Math.round(Math.sin(angle))
                //         ];
                //     }
                //     else
                //     {
                //         cameraMovement.direction = [0, 0]
                //     }
                // }
                // else
                // {
                //     cameraMovement.direction = [0, 0]
                // }
            });
    }

    private getMousePosition = (): vec2 =>
    {
        var mousePosition = this.app.renderer.plugins.interaction.mouse.getLocalPosition(this.app.stage);
        return [mousePosition.x, mousePosition.y];
    }
}
