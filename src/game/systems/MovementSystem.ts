import { System, Entity } from "../../ecs";
import { TransformComponent, MovementComponent } from "../components";
import { vec2 } from "gl-matrix";

type EntityQuery = "entities";
export class MovementSystem extends System<EntityQuery>
{    
    constructor()
    {
        super();
    }

    public query = { entities: [ MovementComponent, TransformComponent ] };
    public run = (
        dt: number, 
        { entities }: Record<EntityQuery, Entity[]>
    ) =>
    {
        entities.forEach(
            entity => 
            {
                const movement = entity.getComponent<MovementComponent>(MovementComponent);
                const transform = entity.getComponent<TransformComponent>(TransformComponent);

                if (!movement || !transform)
                {
                    throw new Error("Something went wrong");
                }

                let timeScaledSpeed = movement.speed * dt;

                let pos = vec2.create();
                vec2.mul(pos, movement.direction, [timeScaledSpeed, timeScaledSpeed]);
                vec2.add(pos, transform.position, pos);

                transform.position = pos;
            });
    }
}
