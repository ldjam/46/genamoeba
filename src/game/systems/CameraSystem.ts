import { System, Entity } from "../../ecs";
import { CameraComponent, TransformComponent } from "../components";

export class CameraSystem extends System<"entities">
{
    constructor()
    {
        super();
    }

    public query = { entities: [ CameraComponent, TransformComponent ] };
    public run = (
        dt: number,
        { entities }: Record<string, Entity[]>
    ) => 
    {
        entities.forEach(
            entity =>
            {
                const transform = entity.getComponent<TransformComponent>(TransformComponent);
                const cameraComponent = entity.getComponent<CameraComponent>(CameraComponent);

                if (!cameraComponent || !transform)
                {
                    return;
                }

                let viewport = cameraComponent.viewport;
                
                viewport.screenWidth = window.innerWidth;
                viewport.screenHeight = window.innerHeight;

                let x = transform.position[0];
                let y = transform.position[1];
                viewport.moveCenter(x, y);
            }
        );
    }
}
