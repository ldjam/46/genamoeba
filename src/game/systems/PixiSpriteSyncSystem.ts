﻿import * as PIXI from "pixi.js";
import { System, Entity } from "../../ecs";
import { SpriteComponent } from "../components/sprite";
import { TransformComponent } from "../components/transform";

const resourceKey: string = "pixi_sprite";

type EntityQuery = "entities";
export class PixiSpriteSyncSystem extends System<EntityQuery>
{    
    constructor(
        private container: PIXI.Container
    ) 
    {
        super();
    }
    
    public query = { entities: [ SpriteComponent, TransformComponent ] };
    public run = (
        dt: number, 
        { entities } : Record<EntityQuery, Entity[]>,
    ) =>
    {
        entities.forEach(entity => {
            const spriteInfo = entity.getComponent<SpriteComponent>(SpriteComponent);
            const transform = entity.getComponent<TransformComponent>(TransformComponent);
            
            if (!spriteInfo || !transform)
            {
                throw new Error("Something went wrong");
            }

            const sprite = spriteInfo.sprite;
            sprite.width = spriteInfo.dimensions[0];
            sprite.height = spriteInfo.dimensions[1];
            sprite.anchor.set(spriteInfo.anchor[0], spriteInfo.anchor[1]);
            sprite.tint = spriteInfo.tint;
            sprite.position = new PIXI.Point(transform.position[0], transform.position[1]);
            //sprite.zIndex = transform.position[2];
            sprite.rotation = transform.rotation;

            this.container.addChild(
                spriteInfo.sprite
            );
        });
    }
}
