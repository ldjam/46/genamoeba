import { System, Entity } from "../../ecs";
import { vec2 } from "gl-matrix";
import { MoveToComponent, TransformComponent, MovementComponent } from "../components";

type EntityQuery = "entities";
export class MoveToSystem extends System<EntityQuery>
{    
    constructor()
    {
        super();
    }

    public query = { entities: [ MoveToComponent ] };
    public run = (
        dt: number, 
        { entities }: Record<EntityQuery, Entity[]>
    ) =>
    {
        entities.forEach(
            entity => 
            {
                const moveToComponent = entity.getComponent<MoveToComponent>(MoveToComponent);
                const movement = entity.getComponent<MovementComponent>(MovementComponent);
                const transform = entity.getComponent<TransformComponent>(TransformComponent);

                if (!movement || !transform || !moveToComponent)
                {
                    throw new Error("Something went wrong");
                }

                var distance = vec2.create();
                vec2.subtract(distance, moveToComponent.target, transform.position);

                if (vec2.length(distance) < 5)
                {
                    // we've reached our destination
                    // just set the position
                    movement.direction = [0, 0];
                    transform.position = moveToComponent.target;

                    entity.removeComponents(MoveToComponent);
                    return;
                }

                var normalizedDirection = vec2.create();
                vec2.normalize(normalizedDirection, distance);

                movement.direction = normalizedDirection;
            });
    }
}
