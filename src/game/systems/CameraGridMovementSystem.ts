import { System, Entity, EntityId } from "../../ecs";
import { CameraComponent, TransformComponent, MoveToComponent } from "../components";
import { GridComponent } from "../../app/components/GridComponent";
import { toPixelPosition } from "../../app/views"
import { GridCell } from "../../app/models"
import { CameraUseGridMovementBehavior } from "../components/CameraUseGridMovementBehavior";

export class CameraGridMovementSystem extends System<"cameras" | "grids">
{
    private lastClickedOnSpace: Record<EntityId, GridCell> = {};

    public query = { 
        cameras: [ CameraComponent, TransformComponent, CameraUseGridMovementBehavior ],
        grids: [ GridComponent ]
    };

    public run = (
        dt: number,
        { cameras, grids }: Record<string, Entity[]>
    ) => 
    {
        const grid = grids[0];
        if (!grid)
        {
            return;
        }

        var gridComponent = grid.getComponent<GridComponent>(GridComponent);
        var selection = gridComponent?.selection;

        if (!selection)
        {
            return;
        }

        cameras.forEach(
            camera =>
            {
                const gridComponent = grid.getComponent<GridComponent>(GridComponent);

                if (!gridComponent?.selection)
                {
                    return;
                }

                var { x, y } = toPixelPosition(gridComponent.selection);

                if 
                (
                    this.lastClickedOnSpace[camera.id] && 
                    this.lastClickedOnSpace[camera.id].col == gridComponent.selection.col &&
                    this.lastClickedOnSpace[camera.id].row == gridComponent.selection.row
                )
                {
                    return;
                }
                else
                {
                    this.lastClickedOnSpace[camera.id] = gridComponent.selection;
                }

                camera
                .addComponents(
                    new MoveToComponent([x, y])
                )
            }
        );
    }
}
