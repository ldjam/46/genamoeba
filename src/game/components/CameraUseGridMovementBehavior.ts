import { Component, EntityId } from "../../ecs";

export class CameraUseGridMovementBehavior extends Component
{
    constructor(
        public gridId?: EntityId
    )
    {
        super();
    }
}
