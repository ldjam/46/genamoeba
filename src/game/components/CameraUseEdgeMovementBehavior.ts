import { Component } from "../../ecs";

const defaultDistance = 50;
export class CameraUseEdgeMovementBehavior extends Component
{
    constructor(
        public top: number = defaultDistance,
        public bottom: number = defaultDistance,
        public left: number = defaultDistance,
        public right: number = defaultDistance
    )
    {
        super();
    }
}
