import { Component, EntityId } from "../../ecs";

export class MoveToComponent extends Component
{
    constructor(
        public target: [number, number] = [0, 0]
    )
    {
        super();
    }
}
