﻿import {Component} from "../../ecs";
import { vec2 } from  "gl-matrix";

export class TransformComponent extends Component
{
    constructor(
        public position: vec2 = vec2.create(),
        public scale: vec2 = vec2.fromValues(1, 1),
        public rotation: number = 0,
        public zIndex: number = 0
    )
    {
        super();
    }
}
