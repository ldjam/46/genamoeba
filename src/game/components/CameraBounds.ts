import {Component} from "../../ecs";

export class CameraBoundsComponent extends Component
{
    constructor(
        public x: number = 0,
        public y: number = 0,
        public width: number  = 3000,
        public height: number = 3000
    )
    {
        super();
    }
}
