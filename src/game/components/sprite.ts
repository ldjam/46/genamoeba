import {Component} from "../../ecs";
import * as PIXI from "pixi.js";

export class SpriteComponent extends Component
{
    // non-serializable
    public readonly sprite: PIXI.Sprite;

    constructor(
        public dimensions: [number, number], 
        public anchor: [number, number] = [0.5, 0.5],
        public tint: number = 0xFFFFFF
    )
    {
        super();

        const sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
        sprite.height = dimensions[0];
        sprite.width = dimensions[1];
        sprite.anchor.x = anchor[0];
        sprite.anchor.y = anchor[1];
        sprite.tint = tint;

        this.sprite = sprite;
    }

    dispose = () =>
    {
        this.sprite.destroy();
    }
}
