import * as PIXI from "pixi.js";
import {Viewport} from "pixi-viewport";
import {Component} from "../../ecs";

export type CameraWorldBounds =
{
    x: number;
    y: number;
    width: number;
    height: number;
};

export class CameraComponent extends Component
{
    constructor(
        public viewport: Viewport
    )
    {
        super();
    }
}
