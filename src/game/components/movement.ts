import { Component } from "../../ecs";
import { vec2 } from "gl-matrix";

export class MovementComponent extends Component 
{
    constructor(
        public speed: number = 0,
        public direction: vec2 = [0, 0]
    ) 
    {
        super();
    }
}
