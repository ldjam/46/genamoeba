import { Component, EntityId } from "../../ecs";

export class FollowTargetComponent extends Component
{
    constructor(
        public target: EntityId,
        public radius: number = 3
    )
    {
        super();
    }
}
