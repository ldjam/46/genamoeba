import { Component, EntityId } from "../../ecs";

export class CameraUseZoomBehavior extends Component
{
    constructor(
        public gridId?: EntityId
    )
    {
        super();
    }
}
