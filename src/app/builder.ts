import * as PIXI from 'pixi.js';
import PIXISound from 'pixi-sound';
import { EntityStore } from '../ecs';
import { music, sprites, sounds } from './assets';
import {
  AMOEBA_SPEED,
  CAMERA_MARGIN_X,
  CAMERA_MARGIN_Y,
  GRID_WIDTH,
  GRID_HEIGHT,
  FOOD_DECAY,
  GENE_DECAY,
  AMOEBA_DECAY, GRID_SIZE
} from './constants';
import {
  GridComponent,
  CellMenuComponent,
  DebuggerComponent,
  MenuComponent,
  CellObjectComponent,
  CellObjectPhysicsComponent,
  CellTransformComponent,
  AmoebaComponent,
  AmoebaTouchComponent,
  GridHoverComponent,
  GridSelectComponent,
  AmoebaTraitsComponent,
  AmoebaMovementComponent,
  CellActionComponent,
  LifeComponent,
  FoodComponent,
  ScoreWatcherComponent, IGridComponent
} from './components';
import {
  createCellMenu,
  createGrid,
  createMenu,
  createDebuggerView,
  createCellSprite,
  createAmoebaView,
  createScoreView, IGrid,
} from './views';
import {
  GridCell,
  GridLookup,
  GeneType,
  getAmoebaKey,
  getGeneTypesFromAmoebaKey,
  AMOEBA_LOOKUP
} from './models';
import { manager as themes } from './themes';
import { CameraComponent } from '../game/components';
import { Viewport } from 'pixi-viewport';
import { GeneComponent } from "./components/GeneComponent";
import { AmoebaPathfindingComponent } from './components/AmoebaPathfindingComponent';

const waitForLoad = (loader : PIXI.Loader) => new Promise(resolve => loader.load(resolve));

type WorldLayer = "background" | "items" | "player" | "grid" | "cellMenu";

export class Builder {
  private worldContainer : PIXI.Container;
  private layers : Record<WorldLayer, PIXI.Container>;
  private gridLookup : GridLookup;
  private ticker : PIXI.Ticker;
  private backgroundSprite? : PIXI.TilingSprite;
  private grid? : IGrid;
  private viewport : Viewport;

  constructor(
    private app : PIXI.Application,
    private store : EntityStore,
  ) {
    this.worldContainer = app.stage;
    this.gridLookup = new GridLookup();
    this.layers = {
      background: this.createLayer(),
      grid: this.createLayer(),
      items: this.createLayer(),
      player: this.createLayer(),
      cellMenu: this.createLayer(),
    };
    this.app.stage.sortableChildren = true;

    // The players have a health status above their heads
    // which means we need to do some isometric rendering...
    this.layers.player.sortableChildren = true;
    this.ticker = new PIXI.Ticker();

    themes.loadThemeFromStorage();
  }

  private createLayer() {
    return this.worldContainer.addChild(new PIXI.Container());
  }

  public async loadAssets() {
    const loader = this.app.loader;
    Object.values(music).forEach(url => loader.add(url));
    Object.values(sounds).forEach(url => loader.add(url));
    Object.values(sprites).forEach(url => loader.add(url));
    Object.entries(AMOEBA_LOOKUP).forEach(([name, url]) => loader.add(name, url));

    await waitForLoad(loader).then(() => {
      PIXISound.volume(music.peaceful, 0.5);

      PIXISound.volume(sounds.boing, 0.1);
      PIXISound.volume(sounds.geneBing, 0.1);
      PIXISound.volume(sounds.pop, 0.5);
      PIXISound.volume(sounds.reproduction, 0.1);
      PIXISound.volume(sounds.deathCry, 0.1);
    });

    return this;
  }

  public createDebugger() {
    const debuggerView = createDebuggerView(this.app);

    this.store.createEntity()
      .addComponents(new DebuggerComponent(debuggerView));

    return this;
  }

  public createBackground() {
    this.backgroundSprite = new PIXI.TilingSprite(this.app.loader.resources[themes.getTheme().BACKGROUND_IMAGE].texture, GRID_WIDTH, GRID_HEIGHT);
    this.layers.background.addChild(this.backgroundSprite);

    return this;
  }

  public createMenu() {
    const menu = createMenu({
      onOpen: () => {
        this.ticker.stop();
        this.viewport.pause = true;
      },
      onClose: () => {
        this.ticker.start();
        this.viewport.pause = false;
      },

      onSwitchTheme: (theme) => {
        themes.setTheme(theme);

        // NOTE: Could improvate this by adding theme watchers :thinking:
        if (this.backgroundSprite) {
          this.backgroundSprite.texture = this.app.loader.resources[themes.getTheme().BACKGROUND_IMAGE].texture ;
        }

        if (this.grid) {
          this.grid.onThemeUpdate();
        }
      }
    });

    this.store
      .createEntity()
      .addComponents(
        new MenuComponent(menu),
        new ScoreWatcherComponent(menu),
      );

    return this;
  }

  public createScoreCounter() {
    const view = createScoreView();

    this.store.createEntity().addComponents(new ScoreWatcherComponent(view));

    return this;
  }

  public createGrid() {
    const entity = this.store.createEntity();

    this.grid = createGrid(entity);
    this.layers.grid.addChild(this.grid.container);
    entity.addComponents(
      new GridComponent(this.grid as IGridComponent),
    );

    return this;
  }

  public createCellMenu(cell : GridCell) {
    const entity = this.store.createEntity();

    const cellMenu = createCellMenu(cell, this.app.loader.resources, {
      anySelect: () => { entity.addComponents(new GridSelectComponent(null)); },
      anyHover: () => { entity.addComponents(new GridHoverComponent(null)); },
      actionSelect: (key, payload) => {
        this.store.createEntity().addComponents(new CellActionComponent(cell, key, payload));
      }
    });
    this.layers.cellMenu.addChild(cellMenu.container);
    entity.addComponents(new CellMenuComponent(cellMenu));

    return this;
  }

  public createCamera() {
    const viewport = new Viewport({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight,
      worldWidth: GRID_WIDTH + CAMERA_MARGIN_X * 2,
      worldHeight: GRID_HEIGHT + CAMERA_MARGIN_Y * 2,
      interaction: this.app.renderer.plugins.interaction,
      passiveWheel: false,
      stopPropagation: true
    });
    viewport.clamp({ direction: 'all' });
    viewport.clampZoom({ minScale: 1, maxScale: 10 });
    viewport.drag();
    viewport.wheel({
      percent: 0.01,
      smooth: 10
    });
    viewport.pinch({
      percent: 0.01
    });

    viewport.moveCorner(10 * GRID_SIZE, 10 * GRID_SIZE);
    viewport.setZoom(2.0);

    // Setup window resizing
    window.addEventListener('resize', () => {
      viewport.resize(window.innerWidth, window.innerHeight);
    });


    // Setup margin by adding a child that's offset from the top
    const viewportMargin = new PIXI.Container();
    viewportMargin.position.set(CAMERA_MARGIN_X, CAMERA_MARGIN_Y);
    viewport.addChild(viewportMargin);

    this.app.stage.addChild(viewport);
    this.updateWorldContainer(viewportMargin);

    const entity = this.store.createEntity();
    entity.addComponents(
      new CameraComponent(viewport),
    );

    this.viewport = viewport;

    return this;
  }

  public createWall(cell : GridCell) {
    const wall = createCellSprite(this.app.loader.resources[sprites.wall].texture, cell);
    this.layers.player.addChild(wall.sprite);

    this.createCellObjectEntity(cell)
      .addComponents(
        new CellObjectPhysicsComponent(true),
        new CellTransformComponent(wall),
      );

    return this;
  }

  public createFood(cell: GridCell) {
    const food = createCellSprite(this.app.loader.resources[sprites.food].texture, cell);
    this.layers.items.addChild(food.sprite);
    
    this.createCellObjectEntity(cell)
      .addComponents(
        new CellObjectPhysicsComponent(false),
        new CellTransformComponent(food),
        new LifeComponent(FOOD_DECAY),
        new FoodComponent(),
      );

    return this;
  }

  public createGene(cell : GridCell, key : GeneType) {
    const gene = createCellSprite(this.app.loader.resources[sprites.helix].texture, cell);
    gene.sprite.tint = key.tint;
    this.layers.items.addChild(gene.sprite);

    this.createCellObjectEntity(cell)
      .addComponents(
        new CellObjectPhysicsComponent(false),
        new CellTransformComponent(gene),
        new GeneComponent(key),
        new LifeComponent(GENE_DECAY),
      );

    return this;
  }

  public createAmoeba(cell : GridCell, requestDna: GeneType[] = []) {
    let texture;

    // amoebaKey - TODO use this amoeba key to generate different traits
    // dna - this is the valid dna generated from the amoeba key (could be different from requestDna).
    const amoebaKey = getAmoebaKey(requestDna.map(x => x.key));
    const dna = getGeneTypesFromAmoebaKey(amoebaKey);

    texture = this.app.loader.resources[amoebaKey].texture;

    const amoeba = createAmoebaView(texture, cell, {
      touch: () => {
        entity.addComponents(AmoebaTouchComponent.instance, new GridSelectComponent(null));
      },
      hover: () => {
        entity.addComponents(new GridHoverComponent(null));
      },
    });
    this.layers.player.addChild(amoeba.container);

    const entity = this.createCellObjectEntity(cell)
      .addComponents(
        new CellObjectPhysicsComponent(true),
        new AmoebaComponent(amoeba, AMOEBA_SPEED, dna),
        new CellTransformComponent(amoeba),
        new AmoebaPathfindingComponent(30),
        new AmoebaTraitsComponent(),
        new AmoebaMovementComponent(),
        new LifeComponent(AMOEBA_DECAY, amoeba),
      );

    return this;
  }

  public getGridLookup() {
    return this.gridLookup;
  }

  public getStore() {
    return this.store;
  }

  public getTicker() {
    return this.ticker;
  }

  private createCellObjectEntity(cell : GridCell) {
    const entity = this.store.createEntity();

    this.gridLookup.set(entity.id, cell);
    entity.addComponents(
      new CellObjectComponent(entity.id, this.gridLookup),
    );

    return entity;
  }

  private updateWorldContainer(container : PIXI.Container) {
    Object.values(this.layers).forEach(layer => {
      this.worldContainer.removeChild(layer);
      container.addChild(layer);
    });

    this.worldContainer = container;
  }
}

export const createBuilder = (app : PIXI.Application, store : EntityStore) => new Builder(app, store);
