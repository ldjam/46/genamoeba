// GRID_SIZE - Size in pixels of a single grid cell
// GRID_ROWS - Number of rows in the grid
// GRID_COLS - Number of cols in the grid
// GRID_WIDTH - Width in pixels of the entire grid
// GRID_HEIGHT - Height in pixels of the entire grid
export const GRID_SIZE = 48;
export const GRID_ROWS = 50;
export const GRID_COLS = 50;
export const GRID_WIDTH = GRID_SIZE * GRID_COLS;
export const GRID_HEIGHT = GRID_SIZE * GRID_ROWS;

export const HEALTH_BAR_WIDTH = GRID_SIZE * 0.75;
export const HEALTH_BAR_HEIGHT = 8;
export const HEALTH_BAR_MIN_WIDTH = 8;

// CAMERA_MARGIN - The number of pixels to add between the grid and the edge of the camera.
// More margin, gives more space for the menu and other UI to pop up.
export const CAMERA_MARGIN_X = 100;
export const CAMERA_MARGIN_Y = 150;

// CELL_MENU_SIZE - Size in pixels of the cell menu items
export const CELL_MENU_ITEM_SIZE = 64;
export const CELL_MENU_ITEM_MARGIN = 12;
export const CELL_MENU_RADIUS = GRID_SIZE * 2;

export const AMOEBA_SPEED = 2;
export const AMOEBA_TOUCH_WAIT = 500;
export const AMOEBA_DECAY = 0.04;
export const AMOEBA_WAIT_MIN = 10;
export const AMOEBA_WAIT_SPREAD = 50;

// AMOEBA_LEAVE_FOOD_CHANCE - The chance that an amoeba leaves food behind when it dies
export const AMOEBA_LEAVE_FOOD_CHANCE = 0.5;

// REPRODUCTION_MAX_COUNT - The max amount of times an amoeba can reproduce after picking up a gene
export const AMOEBA_REPRODUCTION_MAX_COUNT = 5;
export const AMOEBA_REPRODUCTION_WAIT_MIN = 100;
export const AMOEBA_REPRODUCTION_WAIT_SPREAD = 200;

// GENE_REPRODUCTION_INCREAASE - How many reproduction points should picking up a gene give?
export const GENE_REPRODUCTION_INCREASE = 3;

// MAX_LIFE - The max life for anything
export const MAX_LIFE = 100;

// FOOD_LIFE_BOOT - How much life does consuming a food give you (never exceeds max life)
export const FOOD_LIFE_BOOST = 50;

// MID_LIFE - The mid point when amoeba's start to get worries about food
// MID_WEIGHT - At this point, how worried are the amoeba's
export const MID_LIFE = MAX_LIFE / 2;
export const MID_WEIGHT = 1.3;
export const MID_SPEED_ADJ = 1.2;
export const MID_WAIT_ADJ = 1.5;

// PANIC_LIFE - The point when amoeba's are really worried about food
// PANIC_WEIGHT - How worried should amoeba's be about food?
export const PANIC_LIFE = MAX_LIFE / 4;
export const PANIC_WEIGHT = 2;
export const PANIC_SPEED_ADJ = 1.6;
export const PANIC_WAIT_ADJ = 2;

// HAPPE_WEIGHT - When an amoeba is well fed, how interested in food are they?
export const HAPPY_WEIGHT = 0.5;

export const FOOD_DECAY = 0.1;
export const GENE_DECAY = 0.01;

export const STORAGE_KEY_THEME = '_theme';
