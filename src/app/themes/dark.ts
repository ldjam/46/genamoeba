import { backgroundDark } from '../assets/sprites';

export const BACKGROUND_IMAGE = backgroundDark;

export const GRID_LINE_COLOR = 0xffffff;
export const GRID_LINE_ALPHA = 0.3;
// GRID_HOVE - line style, color, and alpha for a hovered grid cell
export const GRID_HOVER_LINE_ALPHA = 0.6;
export const GRID_HOVER_COLOR = 0xffffff;
export const GRID_HOVER_ALPHA = 0.2;
// GRID_SELECT - line style, color, and alpha for a selected grid cell
export const GRID_SELECT_LINE_ALPHA = 0.5;
export const GRID_SELECT_COLOR = 0xffffff;
export const GRID_SELECT_ALPHA = 0.5;
