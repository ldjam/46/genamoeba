import * as darkTheme from './dark';
import * as lightTheme from './light';
import { STORAGE_KEY_THEME } from '../constants';

export type ThemeKey = "dark" | "light";

const createThemeManager = () => {
  let currentThemeKey = "light";
  let currentTheme = lightTheme;

  const setTheme = (theme: ThemeKey) => {
    currentThemeKey = theme;
    
    if (currentThemeKey === "light") {
      currentTheme = lightTheme;
    } else {
      currentTheme = darkTheme;
    }

    saveThemeInStorage(theme);
  };

  const saveThemeInStorage = (theme: ThemeKey) => {
    if (!localStorage) {
      return;
    }

    localStorage.setItem(STORAGE_KEY_THEME, theme);
  };

  const loadThemeFromStorage = () => {
    if (!localStorage) {
      return;
    }
    
    const value = localStorage.getItem(STORAGE_KEY_THEME);

    if (value) {
      setTheme(value as ThemeKey);
    }
  };

  const getTheme = () => {
    return currentTheme;
  };

  const getThemeKey = () => {
    return currentThemeKey;
  };

  return {
    getTheme,
    getThemeKey,
    loadThemeFromStorage,
    setTheme
  };
}

export const manager = createThemeManager();
