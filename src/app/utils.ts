import { EntityStore, Entity, EntityId } from '../ecs';
import { CellObjectPhysicsComponent, CellObjectComponent } from './components';
import { DIRECTIONS, GridCell, GridLookup } from './models';
import { isOutOfBounds } from './views';

export const canMoveToCell = (cell : GridCell, lookup : GridLookup, store : EntityStore, moverId? : EntityId) : boolean => {
  if (isOutOfBounds(cell)) {
    return false;
  }

  const isRigid = lookup.getEntityIds(cell)
    .filter(id => id !== moverId)
    .map(x => store.findEntity(x))
    .some(x => CellObjectPhysicsComponent.getOrDefault(x).isRigid);

  return !isRigid;
}

export const getEmptyNeighbourCell = (entity : Entity, gridLookup : GridLookup, store : EntityStore) => {
  const { cell } = entity.getComponent<CellObjectComponent>(CellObjectComponent)!;

  const available = DIRECTIONS.filter(x => canMove(cell!, x, gridLookup, store));

  if (available.length === 0) {
    return;
  }

  const direction = available[Math.floor(Math.random() * available.length)];

  return {
    row: cell.row + direction.row,
    col: cell.col + direction.col,
  };
}

export function canMove(cell : GridCell, direction : GridCell, lookup : GridLookup, store : EntityStore) {
  const nextCell = {
    row: cell.row + direction.row,
    col: cell.col + direction.col,
  };

  return canMoveToCell(nextCell, lookup, store);
}

export const shuffle = <T> (a : T[]) : T[] => {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
};

export const take = <T> (a : T[], len : number) : T[] => {
  a.length = len;
  return a;
};
