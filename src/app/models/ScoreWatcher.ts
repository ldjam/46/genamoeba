export interface IScoreWatcher {
  updateScore(score: number, collectedDna: Set<string>): void;
  dispose?(): void;
}
