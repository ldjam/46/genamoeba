import { GeneType } from './GeneType';

export type ObjectType = "gene" | "amoeba";
export type CellActionKey = "gene" | "wall" | "food";
