import { EntityId } from "../../ecs";
import { GridCell } from "../models";

const toCellKey = (cell : GridCell) => {
  return `${cell.row}_${cell.col}`;
};

const toCell = (cellKey : string) => {
  const [ row, col ] = cellKey.split('_').map(Number);

  return { row, col };
};

/**
 * This houses the state for looking up
 * what entity owns a grid square (or vice-versa).
 * 
 * It houses a two-way map of cell <=> EntityId
 */
export class GridLookup {
  private cellLookup : Map<string, Set<EntityId>>;
  private idLookup : Map<EntityId, Set<string>>;

  public constructor() {
    this.cellLookup = new Map<string, Set<EntityId>>();
    this.idLookup = new Map<EntityId, Set<string>>();
  }

  public set(id : EntityId, cell : GridCell) : void {
    const cellKey = toCellKey(cell);

    this.deleteEntityId(id);

    this.addCellKeyToId(id, cellKey);
    this.addIdToCellKey(cellKey, id);
  }

  public deleteEntityId(id : EntityId) : void {
    if (!this.idLookup.has(id)) {
      return;
    }

    const cells = this.getCells(id);

    this.idLookup.delete(id);
    cells.forEach(cell => {
      this.deleteIdFromCellKey(toCellKey(cell), id);
    });
  }

  public getCells(id : EntityId) : GridCell[] {
    const cells = this.idLookup.get(id);
    
    return cells ? Array.from(cells).map(toCell) : [];
  }

  public getEntityIds(cell : GridCell) : EntityId[] {
    const cellKey = toCellKey(cell);

    const ids = this.cellLookup.get(cellKey);

    return ids ? Array.from(ids) : [];
  }

  public hasEntityIds(cell : GridCell) : boolean {
    return this.cellLookup.has(toCellKey(cell));
  }

  private deleteIdFromCellKey(cellKey : string, id : EntityId) : void {
    const ids = this.cellLookup.get(cellKey);

    if (!ids) {
      return;
    }

    ids.delete(id);

    if (ids.size <= 0) {
      this.cellLookup.delete(cellKey);
    }
  }

  private addCellKeyToId(id : EntityId, cellKey : string) {
    const cells = this.idLookup.get(id);

    if (!cells) {
      this.idLookup.set(id, new Set<string>([cellKey]));
    } else {
      cells.add(cellKey);
    }
  }

  private addIdToCellKey(cellKey: string, id: EntityId) {
    const ids = this.cellLookup.get(cellKey);

    if (!ids) {
      this.cellLookup.set(cellKey, new Set<EntityId>([id]));
    } else {
      ids.add(id);
    }
  }
}
