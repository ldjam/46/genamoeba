import { LOOKUP } from '../assets/amoebas';

// LOOKUP has no guarantee that the key parts are ordered...
// Let's create a lookup that is ordered.
export const AMOEBA_LOOKUP = Object.keys(LOOKUP).reduce((acc, key) => {
  const orderedKey = key.split('_').sort().join('_');

  return Object.assign(acc, { [orderedKey]: LOOKUP[key] });
}, {}) as Record<string, string>;

export type GeneKey = 'ear' | 'eye' | 'foot' | 'hair' | 'light' | 'mouth' | 'poison' | 'shell' | 'sparkles' | 'wing';
export const GENE_KEYS = ['ear', 'eye', 'foot', 'hair', 'light', 'mouth', 'poison', 'shell', 'sparkles', 'wing'] as GeneKey[];

export interface GeneType {
  readonly key : GeneKey,
  readonly tint : number,
}

export const GENE_TYPES = {
  ear: { key: 'ear', tint: 0x202020 },
  eye: { key: 'eye', tint: 0xf927cf },
  foot: { key: 'foot', tint: 0x542f05 },
  hair: { key: 'hair', tint: 0x898075 },
  light: { key: 'light', tint: 0xfcfc4e },
  mouth: { key: 'mouth', tint: 0xe00808 },
  poison: { key: 'poison', tint: 0x05b52b },
  shell: { key: 'shell', tint: 0x5d05b5 },
  sparkles: { key: 'sparkles', tint: 0xffffff },
  wing: { key: 'wing', tint: 0x1765b7 },
} as Record<GeneKey, GeneType>;

export const GENES = GENE_KEYS.map(key => GENE_TYPES[key]);

export const getAmoebaKey = (dna: GeneKey[]): string => {
  if (!dna.length) {
    return 'amoeba';
  }
  
  const candidate = [...dna].sort().join('_');

  if (AMOEBA_LOOKUP[candidate]) {
    return candidate;
  } else {
    return getAmoebaKey(dna.slice(0, dna.length - 1));
  }
};

export const getGeneTypesFromAmoebaKey = (key: string): GeneType[] => {
  if (!key) {
    return [];
  }
  
  return key.split('_')
    .map(part => GENE_TYPES[part as GeneKey])
    .filter(x => x);
};

export const getGeneKeysFromAmoebaKey = (key: string): GeneKey[] => {
  return getGeneTypesFromAmoebaKey(key).map(x => x.key);
};
