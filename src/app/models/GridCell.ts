export const areEqualCoordinates = (a : GridCell | null, b : GridCell | null) => {
  return a?.row === b?.row && a?.col === b?.col;
};

export const DIRECTION_UP = { row: 1, col: 0 };
export const DIRECTION_DOWN = { row: -1, col: 0 };
export const DIRECTION_LEFT = { row: 0, col: -1 };
export const DIRECTION_RIGHT = { row: 0, col: 1 };

export const DIRECTIONS = [
  DIRECTION_UP,
  DIRECTION_DOWN,
  DIRECTION_LEFT,
  DIRECTION_RIGHT
];

export interface GridCell {
  readonly row : number;
  readonly col : number;
}
