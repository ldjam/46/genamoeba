import { getAmoebaKey } from './GeneType';

describe('GeneType', () => {
  describe('getAmoebaKey', () => {
    it.each`
      input | output
      ${[]} | ${'amoeba'}
      ${['hair']} | ${'hair'}
      ${['eye', 'hair']} | ${'eye_hair'}
      ${['hair', 'eye']} | ${'eye_hair'}
      ${['hair', 'eye', 'shell']} | ${'eye_hair_shell'}
      ${['hair', 'eye', 'poison', 'shell', 'mouth']} | ${'eye_hair'}
      ${['eye', 'mouth', 'light']} | ${'eye_mouth'}
    `('returns a valid amoeba key given the DNA list (input=$input output=$output)', ({ input, output }) => {
      expect(getAmoebaKey(input)).toEqual(output);
    });
  });
});
