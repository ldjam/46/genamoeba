import * as PIXI from "pixi.js";
import { ECS, EntityStore } from "../ecs";
import { createBuilder } from './builder';
import { GridSystem } from "./systems/GridSystem";
import { CellMenuSystem } from "./systems/CellMenuSystem";
import { CameraSystem } from "../game/systems/CameraSystem";
import { MovementSystem } from "../game/systems/MovementSystem";
import { CleanupSystem } from "./systems/CleanupSystem";
import { SpriteRenderingSystem } from "../game/systems/SpriteRenderingSystem";
import { MoveToSystem } from "../game/systems/MoveToSystem";
import { FollowTargetSystem } from "../game/systems/FollowTargetSystem";
import { CellActionSystem } from "./systems/CellActionSystem";
import { CameraLimitToBoundsSystem } from "../game/systems/CameraLimitToBoundsSystem";
import { CameraDragMovementSystem } from "../game/systems/CameraDragMovementSystem";
import { GridMovementSystem } from "./systems/GridMovementSystem";
import { ReproductionSystem } from "./systems/ReproductionSystem";
import { AmoebaBrainSystem } from "./systems/AmoebaBrainSystem";
import { MusicSystem } from "./systems/MusicSystem";
import { CellResetSystem } from "./systems/CellResetSystem";
import { AmoebaTouchSystem } from "./systems/AmoebaTouchSystem";
import { AmoebaPathfindingSystem } from "./systems/AmoebaPathfindingSystem";
import { AmoebaMovementSystem } from "./systems/AmoebaMovementSystem";
import { DebuggerSystem } from "./systems/DebuggerSystem";
import { CellHitSystem } from "./systems/CellHitSystem";
import { LifeSystem } from "./systems/LifeSystem";
import { ScoreSystem } from "./systems/ScoreSystem";
import { AmoebaHungerSystem } from "./systems/AmoebaHungerSystem";
import { CheatSystem } from "./systems/CheatSystem";

async function main()
{
    const el = document.getElementById('main')!;
    const app = new PIXI.Application({
        width: window.innerWidth, height: window.innerHeight, backgroundColor: 0x000000, resolution: 1, antialias: true
    });
    app.renderer.plugins.interaction.moveWhenInside = true;
    app.resizeTo = window;
    el.appendChild(app.view);

    const entityStore =
        new EntityStore();

    const builder = (await createBuilder(app, entityStore).loadAssets())
      // .createDebugger()
      .createCamera()
      .createBackground()
      .createGrid()
      .createMenu()
      .createScoreCounter()
      .createAmoeba({ row: 3, col: 10 })
      .createAmoeba({ row: 10, col: 5 })
      .createAmoeba({ row: 6, col: 14 })
      .createAmoeba({ row: 5, col: 10 })
      .createAmoeba({ row: 6, col: 2 });

    const ecs = new ECS(entityStore)
        .addSystem(new MusicSystem())
        .addSystem(new ScoreSystem())
        .addSystem(new GridSystem())
        .addSystem(new CellMenuSystem(builder))
        .addSystem(new FollowTargetSystem(entityStore))
        .addSystem(new CameraDragMovementSystem(app.stage))
        .addSystem(new MoveToSystem())
        .addSystem(new MovementSystem())
        .addSystem(new CameraSystem())
        .addSystem(new CameraLimitToBoundsSystem())
        .addSystem(new SpriteRenderingSystem())
        // ---- User action systems ----
        .addSystem(new CellActionSystem(builder))
        .addSystem(new AmoebaTouchSystem())
        // ---- Grid movement systems ----
        .addSystem(new AmoebaPathfindingSystem(builder))
        .addSystem(new AmoebaMovementSystem(builder))
        .addSystem(new AmoebaHungerSystem())
        .addSystem(new GridMovementSystem(builder))
        .addSystem(new CellResetSystem())
        .addSystem(new CellHitSystem(builder))
        // ---- Thinking systems ----
        .addSystem(new AmoebaBrainSystem(builder))
        .addSystem(new ReproductionSystem(builder))
        // ---- Clean up systems ----
        .addSystem(new LifeSystem(builder))
        .addSystem(new CleanupSystem())
        .addSystem(new DebuggerSystem(builder));

    if (window.location.hostname === 'localhost') {
        ecs.addSystem(new CheatSystem(builder));
    }

    const ticker = builder.getTicker();
    ticker.add((delta) => {
        ecs.updateSystems(delta);
    });
    ticker.start();
}

document.addEventListener('DOMContentLoaded', main);