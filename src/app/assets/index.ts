import * as music from './music';
import * as sprites from './sprites';
import * as sounds from './sounds';
import * as amoebas from './amoebas';

export {
  music,
  sprites,
  sounds,
  amoebas
};
