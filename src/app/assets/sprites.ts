import background from './background.jpg';
import backgroundDark from './background_dark.jpg';
import credits from './credits.png';
import foodIcon from './icons/food_icon.png';
import food from './icons/food.png';
import helix from './icons/helix_icon.png';
import wallIcon from './icons/wall_icon_small.png';
import wall from './wall.png';

export {
  background,
  backgroundDark,
  credits,
  foodIcon,
  food,
  helix,
  wallIcon,
  wall,
};
