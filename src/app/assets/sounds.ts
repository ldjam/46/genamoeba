import genePositionClick from './sounds/gene_position_click.wav';
import geneBing from './sounds/gene_bing.wav';
import pop from './sounds/pop.wav';
import reproduction from './sounds/reproduction_quiek.wav'
import boing from './sounds/collision_boing.wav';
import deathCry from './sounds/death_cry.wav';

export {
  boing,
  deathCry,
  genePositionClick,
  geneBing,
  pop,
  reproduction
};
