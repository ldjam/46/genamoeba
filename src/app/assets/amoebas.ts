/**
/* DO NOT CHANGE!
/*
/* This file is auto generated by running 
/* npm run build:assets:amoebas
 */
import amoeba from "./amoebas/amoeba.png";
import ear from "./amoebas/ear.png";
import ear_eye from "./amoebas/ear_eye.png";
import ear_eye_foot from "./amoebas/ear_eye_foot.png";
import ear_eye_foot_hair from "./amoebas/ear_eye_foot_hair.png";
import ear_eye_foot_hair_mouth from "./amoebas/ear_eye_foot_hair_mouth.png";
import ear_eye_foot_hair_mouth_shell from "./amoebas/ear_eye_foot_hair_mouth_shell.png";
import ear_eye_foot_hair_shell from "./amoebas/ear_eye_foot_hair_shell.png";
import ear_eye_foot_mouth from "./amoebas/ear_eye_foot_mouth.png";
import ear_eye_foot_mouth_shell from "./amoebas/ear_eye_foot_mouth_shell.png";
import ear_eye_foot_shell from "./amoebas/ear_eye_foot_shell.png";
import ear_eye_hair from "./amoebas/ear_eye_hair.png";
import ear_eye_hair_mouth from "./amoebas/ear_eye_hair_mouth.png";
import ear_eye_hair_shell from "./amoebas/ear_eye_hair_shell.png";
import ear_eye_hair_wing from "./amoebas/ear_eye_hair_wing.png";
import ear_eye_mouth from "./amoebas/ear_eye_mouth.png";
import ear_eye_mouth_shell from "./amoebas/ear_eye_mouth_shell.png";
import ear_eye_mouth_shell_wing from "./amoebas/ear_eye_mouth_shell_wing.png";
import ear_eye_shell from "./amoebas/ear_eye_shell.png";
import ear_eye_sparkles from "./amoebas/ear_eye_sparkles.png";
import ear_foot from "./amoebas/ear_foot.png";
import ear_foot_hair from "./amoebas/ear_foot_hair.png";
import ear_foot_hair_mouth from "./amoebas/ear_foot_hair_mouth.png";
import ear_foot_hair_shell from "./amoebas/ear_foot_hair_shell.png";
import ear_foot_mouth from "./amoebas/ear_foot_mouth.png";
import ear_foot_mouth_poison from "./amoebas/ear_foot_mouth_poison.png";
import ear_foot_mouth_shell from "./amoebas/ear_foot_mouth_shell.png";
import ear_foot_shell from "./amoebas/ear_foot_shell.png";
import ear_hair from "./amoebas/ear_hair.png";
import ear_hair_light_shell from "./amoebas/ear_hair_light_shell.png";
import ear_hair_mouth from "./amoebas/ear_hair_mouth.png";
import ear_hair_mouth_shell from "./amoebas/ear_hair_mouth_shell.png";
import ear_hair_shell from "./amoebas/ear_hair_shell.png";
import ear_mouth from "./amoebas/ear_mouth.png";
import ear_mouth_shell from "./amoebas/ear_mouth_shell.png";
import ear_shell from "./amoebas/ear_shell.png";
import eye from "./amoebas/eye.png";
import eye_foot from "./amoebas/eye_foot.png";
import eye_foot_hair from "./amoebas/eye_foot_hair.png";
import eye_foot_hair_mouth from "./amoebas/eye_foot_hair_mouth.png";
import eye_foot_hair_mouth_shell from "./amoebas/eye_foot_hair_mouth_shell.png";
import eye_foot_hair_poison_shell from "./amoebas/eye_foot_hair_poison_shell.png";
import eye_foot_hair_shell from "./amoebas/eye_foot_hair_shell.png";
import eye_foot_mouth from "./amoebas/eye_foot_mouth.png";
import eye_foot_mouth_shell from "./amoebas/eye_foot_mouth_shell.png";
import eye_foot_mouth_wing from "./amoebas/eye_foot_mouth_wing.png";
import eye_foot_shell from "./amoebas/eye_foot_shell.png";
import eye_hair from "./amoebas/eye_hair.png";
import eye_hair_mouth from "./amoebas/eye_hair_mouth.png";
import eye_hair_mouth_shell from "./amoebas/eye_hair_mouth_shell.png";
import eye_hair_shell from "./amoebas/eye_hair_shell.png";
import eye_light from "./amoebas/eye_light.png";
import eye_mouth from "./amoebas/eye_mouth.png";
import eye_mouth_shell from "./amoebas/eye_mouth_shell.png";
import eye_shell from "./amoebas/eye_shell.png";
import foot from "./amoebas/foot.png";
import foot_hair from "./amoebas/foot_hair.png";
import foot_hair_mouth from "./amoebas/foot_hair_mouth.png";
import foot_hair_mouth_shell from "./amoebas/foot_hair_mouth_shell.png";
import foot_hair_mouth_shell_sparkles from "./amoebas/foot_hair_mouth_shell_sparkles.png";
import foot_hair_shell from "./amoebas/foot_hair_shell.png";
import foot_mouth from "./amoebas/foot_mouth.png";
import foot_mouth_shell from "./amoebas/foot_mouth_shell.png";
import foot_shell from "./amoebas/foot_shell.png";
import hair from "./amoebas/hair.png";
import hair_mouth from "./amoebas/hair_mouth.png";
import hair_mouth_shell from "./amoebas/hair_mouth_shell.png";
import hair_shell from "./amoebas/hair_shell.png";
import light_mouth from "./amoebas/light_mouth.png";
import mouth from "./amoebas/mouth.png";
import mouth_shell from "./amoebas/mouth_shell.png";
import new_amoeba_born_animation_strips from "./amoebas/new_amoeba_born_animation_strips.png";
import shell from "./amoebas/shell.png";

export const LOOKUP = {
  "amoeba": amoeba,
  "ear": ear,
  "ear_eye": ear_eye,
  "ear_eye_foot": ear_eye_foot,
  "ear_eye_foot_hair": ear_eye_foot_hair,
  "ear_eye_foot_hair_mouth": ear_eye_foot_hair_mouth,
  "ear_eye_foot_hair_mouth_shell": ear_eye_foot_hair_mouth_shell,
  "ear_eye_foot_hair_shell": ear_eye_foot_hair_shell,
  "ear_eye_foot_mouth": ear_eye_foot_mouth,
  "ear_eye_foot_mouth_shell": ear_eye_foot_mouth_shell,
  "ear_eye_foot_shell": ear_eye_foot_shell,
  "ear_eye_hair": ear_eye_hair,
  "ear_eye_hair_mouth": ear_eye_hair_mouth,
  "ear_eye_hair_shell": ear_eye_hair_shell,
  "ear_eye_hair_wing": ear_eye_hair_wing,
  "ear_eye_mouth": ear_eye_mouth,
  "ear_eye_mouth_shell": ear_eye_mouth_shell,
  "ear_eye_mouth_shell_wing": ear_eye_mouth_shell_wing,
  "ear_eye_shell": ear_eye_shell,
  "ear_eye_sparkles": ear_eye_sparkles,
  "ear_foot": ear_foot,
  "ear_foot_hair": ear_foot_hair,
  "ear_foot_hair_mouth": ear_foot_hair_mouth,
  "ear_foot_hair_shell": ear_foot_hair_shell,
  "ear_foot_mouth": ear_foot_mouth,
  "ear_foot_mouth_poison": ear_foot_mouth_poison,
  "ear_foot_mouth_shell": ear_foot_mouth_shell,
  "ear_foot_shell": ear_foot_shell,
  "ear_hair": ear_hair,
  "ear_hair_light_shell": ear_hair_light_shell,
  "ear_hair_mouth": ear_hair_mouth,
  "ear_hair_mouth_shell": ear_hair_mouth_shell,
  "ear_hair_shell": ear_hair_shell,
  "ear_mouth": ear_mouth,
  "ear_mouth_shell": ear_mouth_shell,
  "ear_shell": ear_shell,
  "eye": eye,
  "eye_foot": eye_foot,
  "eye_foot_hair": eye_foot_hair,
  "eye_foot_hair_mouth": eye_foot_hair_mouth,
  "eye_foot_hair_mouth_shell": eye_foot_hair_mouth_shell,
  "eye_foot_hair_poison_shell": eye_foot_hair_poison_shell,
  "eye_foot_hair_shell": eye_foot_hair_shell,
  "eye_foot_mouth": eye_foot_mouth,
  "eye_foot_mouth_shell": eye_foot_mouth_shell,
  "eye_foot_mouth_wing": eye_foot_mouth_wing,
  "eye_foot_shell": eye_foot_shell,
  "eye_hair": eye_hair,
  "eye_hair_mouth": eye_hair_mouth,
  "eye_hair_mouth_shell": eye_hair_mouth_shell,
  "eye_hair_shell": eye_hair_shell,
  "eye_light": eye_light,
  "eye_mouth": eye_mouth,
  "eye_mouth_shell": eye_mouth_shell,
  "eye_shell": eye_shell,
  "foot": foot,
  "foot_hair": foot_hair,
  "foot_hair_mouth": foot_hair_mouth,
  "foot_hair_mouth_shell": foot_hair_mouth_shell,
  "foot_hair_mouth_shell_sparkles": foot_hair_mouth_shell_sparkles,
  "foot_hair_shell": foot_hair_shell,
  "foot_mouth": foot_mouth,
  "foot_mouth_shell": foot_mouth_shell,
  "foot_shell": foot_shell,
  "hair": hair,
  "hair_mouth": hair_mouth,
  "hair_mouth_shell": hair_mouth_shell,
  "hair_shell": hair_shell,
  "light_mouth": light_mouth,
  "mouth": mouth,
  "mouth_shell": mouth_shell,
  "new_amoeba_born_animation_strips": new_amoeba_born_animation_strips,
  "shell": shell,
} as Record<string, string>;

export {
  amoeba,
  ear,
  ear_eye,
  ear_eye_foot,
  ear_eye_foot_hair,
  ear_eye_foot_hair_mouth,
  ear_eye_foot_hair_mouth_shell,
  ear_eye_foot_hair_shell,
  ear_eye_foot_mouth,
  ear_eye_foot_mouth_shell,
  ear_eye_foot_shell,
  ear_eye_hair,
  ear_eye_hair_mouth,
  ear_eye_hair_shell,
  ear_eye_hair_wing,
  ear_eye_mouth,
  ear_eye_mouth_shell,
  ear_eye_mouth_shell_wing,
  ear_eye_shell,
  ear_eye_sparkles,
  ear_foot,
  ear_foot_hair,
  ear_foot_hair_mouth,
  ear_foot_hair_shell,
  ear_foot_mouth,
  ear_foot_mouth_poison,
  ear_foot_mouth_shell,
  ear_foot_shell,
  ear_hair,
  ear_hair_light_shell,
  ear_hair_mouth,
  ear_hair_mouth_shell,
  ear_hair_shell,
  ear_mouth,
  ear_mouth_shell,
  ear_shell,
  eye,
  eye_foot,
  eye_foot_hair,
  eye_foot_hair_mouth,
  eye_foot_hair_mouth_shell,
  eye_foot_hair_poison_shell,
  eye_foot_hair_shell,
  eye_foot_mouth,
  eye_foot_mouth_shell,
  eye_foot_mouth_wing,
  eye_foot_shell,
  eye_hair,
  eye_hair_mouth,
  eye_hair_mouth_shell,
  eye_hair_shell,
  eye_light,
  eye_mouth,
  eye_mouth_shell,
  eye_shell,
  foot,
  foot_hair,
  foot_hair_mouth,
  foot_hair_mouth_shell,
  foot_hair_mouth_shell_sparkles,
  foot_hair_shell,
  foot_mouth,
  foot_mouth_shell,
  foot_shell,
  hair,
  hair_mouth,
  hair_mouth_shell,
  hair_shell,
  light_mouth,
  mouth,
  mouth_shell,
  new_amoeba_born_animation_strips,
  shell,
};
