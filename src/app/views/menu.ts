import { ThemeKey } from "../themes";
import { sprites } from "../assets";

export interface IMenuViewListener {
  onOpen?(): void;
  onClose?(): void;
  onSwitchTheme(theme: ThemeKey): void
};

type ViewKey = "menu" | "help" | "lose" | "scoreDetails";
export const createMenu = (listeners? : IMenuViewListener) => {
  let menuOpen = false;
  let hasLost = false;
  
  const menuToggle = document.querySelector('#menuToggle')!;
  const modal = <HTMLElement>document.querySelector('#modal');
  const restartButtons = document.querySelectorAll('.js-restart');
  const viewButtons = modal?.querySelectorAll('.js-view-button');
  const views = {
    menu: modal?.querySelector('.js-view-menu'),
    help: modal?.querySelector('.js-view-help'),
    lose: modal?.querySelector('.js-view-lose'),
    scoreDetails: document.getElementById('js-score-details'),
  };

  const showView = (key: ViewKey) => {
    Object.values(views).forEach(x => {
      if (!x) {
        return;
      }

      x.setAttribute('hidden', 'true');
    });

    views[key]?.removeAttribute('hidden');
  };

  const handleViewButtonClick = (el: HTMLElement) => {
    const view = el.dataset.view as ViewKey;

    showView(view);
  };

  const handleRestartClick = () => {
    location.reload();
  };
  
  const menu = {
    isOpen: {
      get() : boolean {
        return menuOpen;
      }
    },
    toggle() {
      if (!menuOpen) {
        this.open();
      } else {
        this.close();
      }
    },
    open(view: ViewKey = "menu") {
      menuOpen = true;
      menuToggle.innerHTML = 'CLOSE MENU';
      modal?.removeAttribute('hidden');
      showView(view);
      listeners?.onOpen?.();

      // reset credits background
      modal.style.backgroundImage = '';
    },
    close() {
      menuOpen = false;
      menuToggle.innerHTML = 'MENU';
      modal?.setAttribute('hidden', 'true');
      listeners?.onClose?.();
    },
    dispose() {},
    updateScore(score: number) {
      // If we've already lost or the score is greater than 0, there's nothing to do here :)
      if (score > 0 || hasLost) {
        return;
      }

      this.open("lose");

      menuToggle.outerHTML = `<button id="menuToggle" type="button" class="js-restart">Try again</button>`;
      document.getElementById('menuToggle').addEventListener('click', handleRestartClick);

      modal.style.backgroundImage = `url(${sprites.credits})`;
    },
  };

  restartButtons?.forEach(x => x.addEventListener('click', handleRestartClick));
  menuToggle?.addEventListener('click', () => menu.toggle());
  viewButtons?.forEach(x => x.addEventListener('click', () => handleViewButtonClick(x as HTMLElement)));

  const lightThemeButton = document.querySelector('#lightTheme')!;
  lightThemeButton.addEventListener('click', () => listeners?.onSwitchTheme("light"));

  const darkThemeButton = document.querySelector('#darkTheme')!;
  darkThemeButton.addEventListener('click', () => listeners?.onSwitchTheme("dark"));

  const scoreElement = document.getElementById('score');
  scoreElement.addEventListener('click', () => {
    menu.open('scoreDetails');
  });

  return menu;
};
