import * as PIXI from 'pixi.js';
import { GridCell } from '../models';
import { toPixelPosition } from './grid';
import { GRID_SIZE } from '../constants';

const wrapContainer = (
  container: PIXI.Container,
  position : { x: number, y: number } | null = null,
  width : number | null = null,
  height : number | null = null
) => {
  if (position) {
    container.position.set(position.x, position.y);
  }

  if (width !== null) {
    container.width = width;
  }

  if (height !== null) {
    container.height = height;
  }

  return {
    dispose() {
      container.destroy();
    },
    getPosition() {
      return container;
    },
    setPosition({ x, y } : { x: number, y: number }) {
      container.position.set(x, y);

      // isometric perspective update
      container.zIndex = y;
    },
    getSize() {
      return container;
    },
    setSize({ width, height } : { width: number, height: number }) {
      container.width = width;
      container.height = height;
    },
    setZIndex(z: number) {
      container.zIndex = z;
    }
  };
};

export const createContainer = (
  position?: { x: number, y: number },
  width?: number,
  height?: number,
) => {
  const container = new PIXI.Container();  

  return {
    ...wrapContainer(container, position, width, height),
    container
  };
};

export const createCellContainer = (
  cell: GridCell
) => {
  const pos = toPixelPosition(cell);

  return createContainer(
    pos,
    GRID_SIZE,
    GRID_SIZE
  );
};

export const createSprite = (
  texture : PIXI.Texture,
  position?: { x: number, y: number },
  width?: number,
  height?: number,
) => {
  const sprite = new PIXI.Sprite(texture);

  return {
    ...wrapContainer(sprite, position, width, height),
    sprite
  };
};

export const createCellSprite = (
  texture: PIXI.Texture,
  cell: GridCell
) => {
  const pos = toPixelPosition(cell);

  return createSprite(
    texture,
    pos,
    GRID_SIZE,
    GRID_SIZE
  );
};

