import * as PIXI from 'pixi.js';
import PIXISound from 'pixi-sound';
import { OutlineFilter } from 'pixi-filters';
import { createCellContainer } from './sprite';
import { sounds } from '../assets';
import { GeneType, GridCell } from '../models';
import {
  GRID_SIZE,
  HEALTH_BAR_WIDTH,
  HEALTH_BAR_HEIGHT,
  HEALTH_BAR_MIN_WIDTH,
  MAX_LIFE
} from '../constants';
import { manager as themes } from '../themes';
import { IAmoebaModel, ILifeView, ITransformComponent } from "../components";

interface AmoebaViewListeners {
  touch?() : void;

  hover?() : void;
};

const drawHealthBar = (graphics : PIXI.Graphics, life : number = MAX_LIFE) => {
  const ratio = life / MAX_LIFE;
  const width = Math.max(HEALTH_BAR_MIN_WIDTH, HEALTH_BAR_WIDTH * ratio);
  const diff = GRID_SIZE - width;
  const shape = new PIXI.RoundedRectangle(diff / 2, -HEALTH_BAR_HEIGHT * 1.5, width, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT - 2);

  graphics.lineStyle(1, 0x000000);
  const red = Math.round(0xff * (1 - ratio)) * 0x10000;
  const green = Math.round(0xff * ratio) * 0x100;
  graphics.beginFill(red + green);
  graphics.drawShape(shape);
  graphics.endFill();

  return graphics;
};

const createHealthBar = () => {
  const graphics = new PIXI.Graphics();

  return drawHealthBar(graphics);
};

interface AmoebaView extends IAmoebaModel, ILifeView, ITransformComponent {
  // createCellContainer() provides no interface
  [x: string]: any
}

export const createAmoebaView = (texture : PIXI.Texture, cell : GridCell, listeners : AmoebaViewListeners) : AmoebaView => {
  const containerView = createCellContainer(cell);
  const sprite = new PIXI.Sprite(texture);
  sprite.width = GRID_SIZE;
  sprite.height = GRID_SIZE;
  const healthBar = createHealthBar();

  const geneHalo = new PIXI.Graphics();
  geneHalo.alpha = 0.5;

  containerView.container.addChild(geneHalo, sprite, healthBar);
  const hoverFilters = [new OutlineFilter(2, themes.getTheme().GRID_HOVER_COLOR, 2)];

  sprite.interactive = true;
  if (listeners.touch) {
    sprite.on('mousedown', listeners.touch);
    sprite.on('tap', listeners.touch);
  }
  sprite.on('mouseover', () => {
    sprite.filters = hoverFilters;
    listeners.hover?.();
  });
  sprite.on('mouseout', () => {
    sprite.filters = [];
  });

  return {
    ...containerView,
    pet() : void {
      PIXISound.play(sounds.boing);
    },
    pickUpGene(geneType? : GeneType) : void {
      geneHalo.clear();
      
      // If we are actually dropping the gene
      if (!geneType) {
        return;
      }

      geneHalo.beginFill(geneType.tint);
      const radius = GRID_SIZE / 2;
      geneHalo.drawCircle(radius, radius, radius);
      geneHalo.endFill();
      PIXISound.play(sounds.geneBing);
    },
    updateLife(life : number) {
      healthBar.clear();

      if (life <= 0) {
        PIXISound.play(sounds.deathCry);
      } else {
        drawHealthBar(healthBar, life);
      }
    },
  };
};
