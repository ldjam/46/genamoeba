import { IScoreWatcher } from "../models/ScoreWatcher";
import { AMOEBA_LOOKUP } from "../models";

export const createScoreView = () : IScoreWatcher => {
  const scoreElement = document.getElementById('score');

  let highestScore = 0;

  return {
    updateScore(score: number, collectedDna: Set<string>) {
      scoreElement.innerText = `score: ${score}`;

      highestScore = Math.max(highestScore, score);

      const overlay = document.getElementById('js-score-details')!;
      overlay.innerHTML = `
        <h2>Highest Score: ${highestScore}</h2>
        <h2>Collected DNAs:</h2>
        ${Array.from(collectedDna).map(dnaString => `
          <figure class="collected-dna">
            <img src="${AMOEBA_LOOKUP[dnaString]}" aria-hidden="true" />
            <figcaption>${dnaString.split('_').join(', ')}</figcaption>
          </figure>
        `).join('\n')}
      `
    }
  };
};
