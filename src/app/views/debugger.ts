import * as PIXI from 'pixi.js';
import { EntityStore } from '../../ecs';
import { GridComponent, CellObjectComponent, AmoebaComponent } from '../components';

export interface IDebuggerView {
  update(store : EntityStore): void;
}

export const createDebuggerView = (app : PIXI.Application) : IDebuggerView => {
  const root = document.body;
  const el = document.createElement('div');
  el.classList.add('debugger');
  root.appendChild(el);
  const ul = document.createElement('ul');
  el.appendChild(ul);

  const update = (store : EntityStore) => {
    const items = [] as { key: string, value: string}[];

    const [gridEntity] = store.query(GridComponent);
    if (gridEntity) {
      const { selection, hovered } = gridEntity.getComponent<GridComponent>(GridComponent)!;
      
      items.push({ key: 'grid.selection', value: JSON.stringify(selection) });
      items.push({ key: 'grid.hovered', value: JSON.stringify(hovered) });
    }

    const gridObjects = store.query(CellObjectComponent);
    gridObjects
      .map(x => ({ cell: x.getComponent<CellObjectComponent>(CellObjectComponent)!.cell, id: x.id }))
      .forEach(x => {
        items.push({ key: `grid[${x.cell.row},${x.cell.col}].entity`, value: x.id.toString() });
      });

    const amoebaObjects = store.query(AmoebaComponent);
    amoebaObjects.forEach(entity => {
      const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;

      items.push({ key: `amoeba[${entity.id}].canReproduce`, value: amoeba.canReproduce().toString() });
      items.push({ key: `amoeba[${entity.id}].isWaiting`, value: amoeba.isWaiting().toString() });
      items.push({ key: `amoeba[${entity.id}].isReproductionWaiting`, value: amoeba.isReproductionWaiting().toString() });
      items.push({ key: `amoeba[${entity.id}].dna`, value: amoeba.dna.toString() });
    });
    
    items.sort((a, b) => b.key.localeCompare(a.key));
    // update list
    ul.innerHTML = '';
    items.forEach(({ key, value }) => {
      const li = document.createElement('li');
      li.textContent = `${key}: ${value}`;
      ul.appendChild(li);
    });
  };

  return {
    update,
  };
};
