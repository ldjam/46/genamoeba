import * as PIXI from 'pixi.js';
import { Entity } from '../../ecs';
import { GridSelectComponent } from '../components/GridSelectComponent';
import { GridHoverComponent } from '../components/GridHoverComponent';
import {
  GRID_SIZE,
  GRID_ROWS,
  GRID_COLS,
  GRID_HEIGHT,
  GRID_WIDTH,
} from '../constants';
import { GridCell, areEqualCoordinates } from '../models';
import { manager as themes } from '../themes';
import { IGridComponent } from "../components";

export const toGridPosition = (pointerX : number, pointerY : number) => {
  const row = Math.floor(pointerY / GRID_SIZE);
  const col = Math.floor(pointerX / GRID_SIZE);

  return { row, col } as GridCell;
}

export const toPixelPosition = ({ row, col } : GridCell) => {
  return {
    x: col * GRID_SIZE,
    y: row * GRID_SIZE,
  };
}

export const isOutOfBounds = ({ row, col } : GridCell) => {
  return row < 0 || col < 0 || row > GRID_ROWS || col > GRID_COLS;
}

const drawGridLines = (graphics: PIXI.Graphics) => {
  const theme = themes.getTheme();

  graphics.lineStyle(1, theme.GRID_LINE_COLOR, theme.GRID_LINE_ALPHA);
  for(let x = 0; x < GRID_WIDTH; x += GRID_SIZE) {
    graphics.moveTo(x, 0);
    graphics.lineTo(x, GRID_HEIGHT);
  }

  for(let y = 0; y < GRID_HEIGHT; y += GRID_SIZE) {
    graphics.moveTo(0, y);
    graphics.lineTo(GRID_WIDTH, y);
  }
};

const createHoverGraphics = (cell : GridCell) => {
  const theme = themes.getTheme();
  const graphics = new PIXI.Graphics();      

  const { x, y } = toPixelPosition(cell);

  graphics.lineStyle(1, theme.GRID_LINE_COLOR, theme.GRID_HOVER_LINE_ALPHA);
  graphics.beginFill(theme.GRID_HOVER_COLOR, theme.GRID_HOVER_ALPHA);
  graphics.drawRect(x, y, GRID_SIZE, GRID_SIZE);
  graphics.endFill();

  return graphics;
};

const createSelectionGraphics = (cell : GridCell) => {
  const theme = themes.getTheme();
  const graphics = new PIXI.Graphics();      

  const { x, y } = toPixelPosition(cell);

  graphics.lineStyle(1, theme.GRID_LINE_COLOR, theme.GRID_SELECT_LINE_ALPHA);
  graphics.beginFill(theme.GRID_SELECT_COLOR, theme.GRID_SELECT_ALPHA);
  graphics.drawRect(x, y, GRID_SIZE, GRID_SIZE);
  graphics.endFill();

  return graphics;
};

export interface IGrid extends IGridComponent {
  container: PIXI.Container;
  onThemeUpdate(): void;
}

export const createGrid = (entity : Entity) : IGrid => {
    let selection : GridCell | null = null;
    let hovered : GridCell | null = null;
    let selectionGraphics : PIXI.Graphics | null = null;
    let hoveredGraphics : PIXI.Graphics | null = null;
    const container = new PIXI.Container();

    const clearSelection = () => {
      selection = null;
      if (selectionGraphics) {
        selectionGraphics.destroy();
        selectionGraphics = null;
      }
    };

    const clearHover = () => {
      hovered = null;
      if (hoveredGraphics) {
        hoveredGraphics.destroy();
        hoveredGraphics = null;
      }
    };

    const select = (coord : GridCell | null) => {
      if (areEqualCoordinates(coord, selection)) {
        return;
      }

      clearHover();
      clearSelection();

      if (coord) {
        selection = coord;
        selectionGraphics = createSelectionGraphics(coord);
        container.addChild(selectionGraphics);
      }
    };

    const hover = (coord : GridCell | null) => {
      if (areEqualCoordinates(coord, hovered)) {
        return;
      }

      clearHover();

      if (coord) {
        hovered = coord;
        hoveredGraphics = createHoverGraphics(coord);
        container.addChild(hoveredGraphics);
      }
    };

    const requestHover = (co : GridCell | null) => {
      entity.addComponents(new GridHoverComponent(co));
    };

    const requestSelect = (co : GridCell | null) => {
      entity.addComponents(new GridSelectComponent(co));
    };

    const onMouseMove = (mouseData : any) => {
      const { x, y } = mouseData.data.getLocalPosition(container);
      const cell = toGridPosition(x, y);

      if (isOutOfBounds(cell)) {
        requestHover(null);
      }
      
      requestHover(cell);
    };

    const onMouseDown = (mouseData : any) => {
      const { x, y } = mouseData.data.getLocalPosition(container);
      const cell = toGridPosition(x, y);
      
      if (isOutOfBounds(cell)) {
        return;
      }

      requestSelect(cell);
    };

    const lineGraphics = new PIXI.Graphics();
    drawGridLines(lineGraphics);
    container.addChild(lineGraphics);
    container.hitArea = new PIXI.Rectangle(0, 0, GRID_WIDTH, GRID_HEIGHT);
    container.interactive = true;
    container.on('mousemove', onMouseMove);
  container.on('mousedown', onMouseDown);
  container.on('tap', onMouseDown);

    return {
      container,
      dispose() {
        this.container.destroy();
      },
      select,
      hover,
      get selection() {
        return selection;
      },
      get hovered() {
        return hovered;
      },
      onThemeUpdate() {
        lineGraphics.clear();
        drawGridLines(lineGraphics);
      }
    };
}
