import * as PIXI from 'pixi.js';
import PIXISound from 'pixi-sound';
import { GridCell, GENES, CellActionKey, GENE_TYPES } from '../models';
import { toPixelPosition } from './grid';
import * as assets from '../assets';
import {
  CELL_MENU_ITEM_SIZE,
  CELL_MENU_ITEM_MARGIN,
  GRID_SIZE,
  CELL_MENU_RADIUS,
} from '../constants';
import { manager as themes } from '../themes';
import { shuffle, take } from '../utils';

const ANGLE_STEP = -Math.PI / 4;
const ANGLE_START = Math.PI - ANGLE_STEP;

type CellMenuItemInfo = {
  icon : string,
  tint? : number,
  onSelect? : CallableFunction
  text?: string,
};

const createMenuItemShape = (width : number) => {
  const itemWidth = width;
  const itemHeight = width;
  const shape = new PIXI.RoundedRectangle(-itemWidth / 2, -itemHeight / 2, itemWidth, itemHeight, 10);

  return shape;
};

const drawMenuItemBackground = (graphics : PIXI.Graphics, shape : PIXI.RoundedRectangle, isHover : boolean = false) => {
  const alpha = isHover ? 0.9 : 0.5;

  graphics.clear();
  graphics.beginFill(themes.getTheme().GRID_SELECT_COLOR, alpha);
  graphics.drawShape(shape);
  graphics.endFill();

  return graphics;
};

const moveToGridCenter = (container : PIXI.Container, cell : GridCell) => {
  const halfGridSize = GRID_SIZE / 2;
  const { x, y } = toPixelPosition(cell);

  container.position.set(x + halfGridSize, y + halfGridSize);

  return container;
};

const createMenuGraphics = (menus : CellMenuItemInfo[], resources : PIXI.IResourceDictionary, listeners : CellMenuEvents) => {
  const theme = themes.getTheme();
  const container = new PIXI.Container();

  const hoverText = new PIXI.Text("", { fill: theme.GRID_HOVER_COLOR });
  hoverText.anchor.set(0.5, 0.5);
  container.addChild(hoverText);

  const width = CELL_MENU_ITEM_SIZE;
  const radius = CELL_MENU_RADIUS;
  const menuItemShape = createMenuItemShape(width);
  menus.forEach(({ icon, tint, text, onSelect }, index) => {
    const angle = ANGLE_START + ANGLE_STEP * index;
    const x = Math.sin(angle) * radius;
    const y = Math.cos(angle) * radius;
    const menuItem = new PIXI.Container();
    menuItem.position.set(x, y);

    const graphics = drawMenuItemBackground(new PIXI.Graphics(), menuItemShape);
    menuItem.addChild(graphics);

    const sprite = new PIXI.Sprite(resources[icon].texture);
    if (tint) {
      sprite.tint = tint;
    }
    sprite.width = width - CELL_MENU_ITEM_MARGIN;
    sprite.height = width - CELL_MENU_ITEM_MARGIN;
    sprite.anchor.set(0.5, 0.5);
    menuItem.addChild(sprite);

    menuItem.interactive = true;
    menuItem.buttonMode = true;
    menuItem.hitArea = menuItemShape;

    menuItem.on('mouseover', () => {
      drawMenuItemBackground(graphics, menuItemShape, true);
      PIXISound.play(assets.sounds.pop);
      listeners.anyHover?.();
      hoverText.text = text || '';
    });
    menuItem.on('mouseout', () => {
      drawMenuItemBackground(graphics, menuItemShape, false);
      hoverText.text = '';
    });
    if (onSelect) {
      menuItem.on('mouseup', onSelect);
      menuItem.on('tap', onSelect);
    }

    container.addChild(menuItem);
  });

  return container;
};

type CellMenuEvents = {
  anyHover? : () => void,
  anySelect? : () => void,
  actionSelect? : (action : CellActionKey, payload? : any) => void,
};
export const createCellMenu = (cell : GridCell, resources : PIXI.IResourceDictionary, listeners : CellMenuEvents) => {
  let menu : PIXI.Container | null = null;
  const container = moveToGridCenter(new PIXI.Container(), cell);

  const showMenu = (menus : CellMenuItemInfo[]) => {
    if (menu) {
      menu.destroy();
    }

    menu = createMenuGraphics(menus, resources, listeners);
    container.addChild(menu);
  };

  const randomGenes = take(shuffle([...GENES]), 8);

  const geneMenus = randomGenes.map(key => ({
    icon: assets.sprites.helix,
    tint: key.tint,
    text: key.key,
    onSelect() {
      PIXISound.play(assets.sounds.genePositionClick);
      listeners.actionSelect?.("gene", key);
      listeners.anySelect?.();
    }
  }));

  const rootMenus = [
    {
      icon: assets.sprites.helix,
      onSelect() {
        showMenu(geneMenus);
      }
    },
    {
      icon: assets.sprites.wallIcon,
      onSelect() {
        listeners.actionSelect?.("wall");
        listeners.anySelect?.();
      }
    },
    {
      icon: assets.sprites.foodIcon,
      onSelect() {
        listeners.actionSelect?.("food");
        listeners.anySelect?.();
      }
    },
  ] as CellMenuItemInfo[];

  showMenu(rootMenus);

  return {
    get container() {
      return container;
    },
    get cell() {
      return cell;
    },
    dispose() {
      this.container.destroy();
    },
  };
};