import { System, Entity } from '../../ecs';
import { AmoebaComponent, ScoreWatcherComponent } from '../components';
import { getAmoebaKey, AMOEBA_LOOKUP } from "../models";

type EntityQuery = "amoebas" | "watchers";

export class ScoreSystem extends System<EntityQuery> {
  private lastScore : number = -1;
  private collectedDna : Set<string> = new Set<string>();

  public query = { amoebas: [AmoebaComponent], watchers: [ScoreWatcherComponent] };
  public run = (dt : number, { amoebas, watchers } : Record<EntityQuery, Entity[]>) => {
    const amoebaDnas = amoebas.map(entity => {
      const { dna } = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
      return dna;
    });

    const score = amoebaDnas
      .map(dna => Math.pow(dna.length + 1, 2))
      .reduce((a, b) => a + b, 0);

    if (score === this.lastScore) {
      return;
    }

    amoebaDnas
      .filter(dna => dna.length > 0)
      .map(dna => getAmoebaKey(dna.map(x => x.key)))
      .filter(dnaString => Boolean(AMOEBA_LOOKUP[dnaString]))
      .forEach(dnaString => this.collectedDna.add(dnaString));

    this.lastScore = score;
    watchers.forEach(x => {
      const watcher = x.getComponent<ScoreWatcherComponent>(ScoreWatcherComponent)!;

      watcher.updateScore(score, this.collectedDna);
    });
  };
}
