import { Entity, System } from "../../ecs";
import { AmoebaComponent } from "../components";
import PIXISound from "pixi-sound";
import * as assets from "../assets";

type EntityQuery = "amoebas";

export class MusicSystem extends System<EntityQuery> {
  constructor() {
    super();

    PIXISound.play(assets.music.peaceful, { loop: true });
  }

  public query = { amoebas: [AmoebaComponent] };
  public run = (dt : number, { amoebas } : Record<EntityQuery, Entity[]>) => {

  };
}
