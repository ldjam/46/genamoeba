import { System, Entity } from '../../ecs';
import { Builder } from '../builder';
import { CellHitComponent, AmoebaComponent, GeneComponent, FoodComponent, LifeComponent } from '../components';
import { GENE_REPRODUCTION_INCREASE } from '../constants';

type EntityQuery = "hits";
export class CellHitSystem extends System<EntityQuery> {
  constructor(private readonly builder : Builder) {
    super();
  }

  public query = { hits: [CellHitComponent] };
  public run = (dt : number, { hits } : Record<EntityQuery, Entity[]>) => {
    const lookup = this.builder.getGridLookup();
    const store = this.builder.getStore();

    hits.forEach(entity => {
      const hit = entity.getComponent<CellHitComponent>(CellHitComponent)!;
      entity.removeComponents(CellHitComponent);

      const occupants = lookup.getEntityIds(hit.cell).map(x => store.findEntity(x));

      const amoebaEntities = occupants.filter(x => x?.getComponent<AmoebaComponent>(AmoebaComponent));
      const geneEntities = occupants.filter(x => x?.getComponent<GeneComponent>(GeneComponent));
      const foodEntities = occupants.filter(x => x?.getComponent<FoodComponent>(FoodComponent));

      amoebaEntities.forEach(amoebaEntity => {
        const amoeba = amoebaEntity!.getComponent<AmoebaComponent>(AmoebaComponent)!;

        geneEntities.forEach(gene => {
          const geneComponent = gene?.getComponent<GeneComponent>(GeneComponent);
          amoeba!.pickUpGene(geneComponent!.geneType);
          gene?.destroy();
        });

        foodEntities.forEach(foodEntity => {
          const food = foodEntity?.getComponent<FoodComponent>(FoodComponent)!;
          food.provideNutritionsTo(amoebaEntity!);
          foodEntity?.destroy();
        });
      });
    });
  };
}
