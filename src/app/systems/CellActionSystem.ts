import { System, Entity } from '../../ecs';
import {
  AmoebaComponent,
  CellActionComponent,
  FoodComponent,
  LifeComponent,
  RemoveIfEmptyComponent
} from '../components';
import { Builder } from '../builder';
import { GridCell, CellActionKey, GeneType } from '../models';

type EntityQuery = "actions";

export class CellActionSystem extends System<EntityQuery> {
  constructor(private builder : Builder) {
    super();
  }

  public query = { actions: [CellActionComponent] };
  public run = (dt : number, { actions } : Record<EntityQuery, Entity[]>) => {
    const lookup = this.builder.getGridLookup();

    actions.forEach(entity => {
      const { cell, action, payload } = entity.getComponent<CellActionComponent>(CellActionComponent)!;

      entity.removeComponents(CellActionComponent).addComponents(RemoveIfEmptyComponent.instance);

      // prevent two entities on same cell
      if (lookup.hasEntityIds(cell)) {
        const occupants = lookup.getEntityIds(cell).map(x => this.builder.getStore().findEntity(x));

        if (action === "gene") {
          const amoebaComponents = occupants.map(x => x?.getComponent<AmoebaComponent>(AmoebaComponent)).filter(Boolean);
          amoebaComponents.forEach(amoeba => {
            amoeba?.pickUpGene(payload as GeneType);
          });
        } else if (action === "food") {
          const amoebaEntities = occupants.filter(x => x?.getComponent<AmoebaComponent>(AmoebaComponent));

          const foodComponent = new FoodComponent();
          amoebaEntities.forEach(entity => {
            foodComponent.provideNutritionsTo(entity!);
          });
        }

        return;
      }

      this.doCellAction(cell, action, payload);
    });
  };

  private doCellAction(cell : GridCell, action : CellActionKey, payload? : any) {
    switch (action) {
      case "wall":
        return this.builder.createWall(cell);
      case "gene":
        return payload ? this.builder.createGene(cell, payload as GeneType) : this.builder;
      case "food":
        return this.builder.createFood(cell);
      default:
        return this.builder;
    }
  }
}
