import { System, Entity } from '../../ecs';
import { AmoebaComponent, CellVelocityComponent, CellObjectComponent } from '../components';
import { DIRECTIONS, GridCell } from '../models';
import { Builder } from '../builder';
import { AmoebaPathfindingComponent } from '../components/AmoebaPathfindingComponent';
import { AmoebaPathfindingSystem } from './AmoebaPathfindingSystem';
import { AmoebaMovementComponent } from '../components/AmoebaMovementComponent';
import { canMoveToCell } from '../utils';

type EntityQuery = "amoebas";
export class AmoebaMovementSystem extends System<EntityQuery> {
    constructor(private builder : Builder) {
        super();
    }

    public query = { 
        amoebas: [AmoebaComponent, CellObjectComponent, AmoebaMovementComponent] 
    };

    public run = (
        dt : number, 
        { amoebas } : Record<EntityQuery, Entity[]>
    ) => 
    {
        amoebas.forEach(x => this.moveAmoeba(x, dt));
    };

    private moveAmoeba(
        entity: Entity, 
        dt: number
    )
    {
        const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
        const cellInfo = entity.getComponent<CellObjectComponent>(CellObjectComponent)!;
        const movement = entity.getComponent<AmoebaMovementComponent>(AmoebaMovementComponent)!;
        
        if (entity.getComponent(CellVelocityComponent)) 
        {
            return;
        }

        if (!amoeba.isWaiting()) {
            amoeba.startWaiting();
            return;
        }

        amoeba.decreaseWait(dt);

        if (amoeba.isWaiting()) {
            return;
        }

        if (movement.nextDirection.col == 0 && movement.nextDirection.row == 0)
        {
            return;
        }

        if (this.canMove(cellInfo.cell, movement.nextDirection))
        {
            entity.addComponents(
                new CellVelocityComponent(
                    movement.nextDirection,
                    amoeba.speed));
        }

        return;
    }

    private canMove(cell : GridCell, direction : GridCell) {
        const lookup = this.builder.getGridLookup();
        const store = this.builder.getStore();
        
        const nextCell = {
          row: cell.row + direction.row,
          col: cell.col + direction.col,
        };

        return canMoveToCell(nextCell, lookup, store);
    }
}
