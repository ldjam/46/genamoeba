import { System, Entity } from '../../ecs';
import { RemoveIfEmptyComponent } from '../components/RemoveIfEmptyComponent';


type EntityQuery = "removals";
/**
 * This is an important system which removes entities that are flagged and
 * no longer have components.
 */
export class CleanupSystem extends System<EntityQuery> {
  public query = { removals: [RemoveIfEmptyComponent] };
  public run = (dt: number, { removals } : Record<EntityQuery, Entity[]>) => {
    removals.forEach(entity => {
      entity.removeComponents(RemoveIfEmptyComponent);

      if (!entity.hasComponents()) {
        entity.destroy();
      }
    });
  }
}
