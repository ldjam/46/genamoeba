import { System, Entity } from '../../ecs';
import {
  AmoebaComponent,
  AmoebaTraitsComponent,
  CellObjectComponent,
  CellVelocityComponent,
  FoodComponent,
  GeneComponent,
  GridTargetComponent,
  LifeComponent,
} from '../components';
import { GridCell } from '../models';
import { Builder } from '../builder';
import { MID_LIFE, PANIC_LIFE, MAX_LIFE, PANIC_WEIGHT, MID_WEIGHT, HAPPY_WEIGHT } from '../constants';

type EntityQuery = "amoebas";
export class AmoebaBrainSystem extends System<EntityQuery> {
  constructor(private builder : Builder) {
    super();
  }

  public query = { amoebas: [AmoebaComponent, CellObjectComponent, AmoebaTraitsComponent] };
  public run = (dt : number, { amoebas } : Record<EntityQuery, Entity[]>) => {
    amoebas.forEach(x => this.thinkForAmoeba(x));
  };

  private thinkForAmoeba(entity : Entity) {
    const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
    const traits = entity.getComponent<AmoebaTraitsComponent>(AmoebaTraitsComponent)!;
    const cellInfo = entity.getComponent<CellObjectComponent>(CellObjectComponent)!;
    const currentCellLocation = cellInfo.cell!;

    if(amoeba.isWaiting() || entity.getComponent(CellVelocityComponent))
    {
        return;
    }

    let searchSize = traits.observableRange;
    let highScore = 0;
    let possibleDestinations: GridCell[] = [currentCellLocation];

    const positionOffset = {
        col: Math.max(currentCellLocation.col - searchSize/2, 0),
        row: Math.max(currentCellLocation.row - searchSize/2, 0)
    };

    for (let iCol = positionOffset.col; iCol < searchSize + positionOffset.col; iCol++)
    {
      for (let iRow = positionOffset.row; iRow < searchSize + positionOffset.row; iRow++)
      {
        let gridCell: GridCell = { col: iCol, row: iRow };

        if(gridCell.col === currentCellLocation.col && gridCell.row === currentCellLocation.row)
        {
          continue;
        }

        var cellScore = this.getGridCellScore(traits, entity, currentCellLocation, gridCell);

        if (cellScore > highScore)
        {
          highScore = cellScore;
          possibleDestinations = [ gridCell ];
        }
        else if (cellScore == highScore)
        {
          possibleDestinations.push(gridCell);
        }
      }
    }

    let selectedDestination = getRandomArrayElement(possibleDestinations);

    if (selectedDestination != currentCellLocation)
    {
      entity
      .addComponents(
        new GridTargetComponent(selectedDestination)
      );
    }
  }

  private getGridCellScore(
    traits: AmoebaTraitsComponent,
    entity: Entity,
    currentCell: GridCell,
    gridCell: GridCell
  )
  {
    let score = 1;

    const gridLookup = this.builder.getGridLookup();
    const store = this.builder.getStore();
    const life = entity.getComponent<LifeComponent>(LifeComponent)?.life || MAX_LIFE;

    gridLookup.getEntityIds(gridCell).forEach((memberEntityId) => {
        if (store.hasComponentAtEntity(memberEntityId, AmoebaComponent))
        {
          score += traits.aggressionScore;
          score -= traits.timidityScore;
        }
        else if (store.hasComponentAtEntity(memberEntityId, GeneComponent))
        {
          score += traits.geneScore;
        }
        else if (store.hasComponentAtEntity(memberEntityId, FoodComponent))
        {
          const hungerWeight = life < PANIC_LIFE ? PANIC_WEIGHT : life < MID_LIFE ? MID_WEIGHT : HAPPY_WEIGHT;

          score += (traits.hungerScore * hungerWeight);
        }
    });

    const distance = getDistance(currentCell, gridCell);
    score -= distance * traits.moveCostFactor;

    return score;
  }
}

function getRandomArrayElement<T>(
  arr: T[]
)
{
  return arr[Math.floor(Math.random() * arr.length)];
}

const getDistance = (a : GridCell, b : GridCell) => {
  if (a.col === b.col && a.row === b.row)
  {
    return 0;
  }

  return Math.sqrt(Math.pow(b.col - a.col, 2) + Math.pow(b.row - a.row, 2));
};
