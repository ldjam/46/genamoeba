import { System, Entity } from '../../ecs';
import { LifeComponent, AmoebaComponent, CellObjectComponent } from '../components';
import { Builder } from '../builder';
import { AMOEBA_LEAVE_FOOD_CHANCE } from '../constants';

type EntityQuery = "lives";
export class LifeSystem extends System<EntityQuery> {
  constructor(private readonly builder: Builder) {
    super();
  }

  public query = { lives: [LifeComponent] };
  public run = (dt: number, { lives } : Record<EntityQuery, Entity[]>) => {
    lives.forEach(entity => {
      const lifeComponent = entity.getComponent<LifeComponent>(LifeComponent)!;

      lifeComponent.life += dt * -lifeComponent.decay;

      if (!lifeComponent.isAlive()) {
        // NOTE: Temporary coupling of amoeba's and life things here...
        this.handleAmoebaDie(entity);

        entity.destroy();
      }
    });
  };

  private handleAmoebaDie(entity: Entity) {
    const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent);
    const { cell } = entity.getComponent<CellObjectComponent>(CellObjectComponent) || {};  

    if (!amoeba || !cell) {
      return;
    }

    if (Math.random() - AMOEBA_LEAVE_FOOD_CHANCE <= 0) {
      this.builder.createFood(cell);
    }
  }
}