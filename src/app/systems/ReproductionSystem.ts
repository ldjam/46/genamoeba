import { System, Entity } from '../../ecs';
import { AmoebaComponent, CellObjectComponent } from '../components';
import { DIRECTIONS, GridCell } from '../models';
import { Builder } from '../builder';
import PIXISound from "pixi-sound";
import { sounds } from "../assets";
import { getEmptyNeighbourCell } from '../utils';

type EntityQuery = "amoebas";

export class ReproductionSystem extends System<EntityQuery> {
  constructor(private builder : Builder) {
    super();
  }

  public query = { amoebas: [AmoebaComponent] };
  public run = (dt : number, { amoebas } : Record<EntityQuery, Entity[]>) => {
    amoebas.forEach((entity) => {
      const amoebaComponent = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;

      // We can't reproduce so don't do anything :|
      if (!amoebaComponent.canReproduce()) {
        return;
      }

      amoebaComponent.decreaseReproductionWait(dt);

      if (amoebaComponent.isReproductionWaiting()) {
        return;
      }

      const cell = getEmptyNeighbourCell(entity, this.builder.getGridLookup(), this.builder.getStore());

      // We want to have a baby and there is room! Let's do it!
      if (cell) {
        this.reproduce(amoebaComponent, cell);
      }

      // Either way (room or no room), penalize the amoeba
      amoebaComponent.pingReproduction(-1);

      // Drop gene if penalizing means we can no longer produce
      if (!amoebaComponent.canReproduce()) {
        amoebaComponent.dropGene();
      }
    });
  };

  private reproduce(parentAmoebaComponent : AmoebaComponent, cell: GridCell) {
    let { dna } = parentAmoebaComponent;

    if (parentAmoebaComponent.carriedGene) {
      dna = dna.concat(parentAmoebaComponent.carriedGene);
    }

    this.builder.createAmoeba(cell, dna);
    PIXISound.play(sounds.reproduction);
  }
}