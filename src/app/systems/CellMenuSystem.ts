import { System, Entity } from "../../ecs";
import { CellMenuComponent, GridComponent } from "../components";
import { Builder } from "../builder";
import { areEqualCoordinates } from "../models";

type EntityQuery = "grids" | "menus";
export class CellMenuSystem extends System<EntityQuery> {
  constructor(private builder : Builder) {
    super();
  };

  public query = { grids: [GridComponent], menus: [CellMenuComponent] };
  public run = (dt : number, { grids, menus }: Record<EntityQuery, Entity[]>) => {
    const gridLookup = this.builder.getGridLookup();
    const cellMenuEntity = menus[0];
    const cellMenu = cellMenuEntity?.getComponent<CellMenuComponent>(CellMenuComponent);

    grids.forEach(entity => {
      const grid = entity.getComponent<GridComponent>(GridComponent)!;

      if (areEqualCoordinates(grid.selection, cellMenu ? cellMenu.cell : null)) {
        // no changes, so do nothing

        return;
      } else if (cellMenuEntity) {
        // open menu is in wrong place now

        cellMenuEntity.destroy();
      }

      if (grid.selection && !gridLookup.hasEntityIds(grid.selection)) {
        this.builder.createCellMenu(grid.selection);
      }
    });
  }
}
