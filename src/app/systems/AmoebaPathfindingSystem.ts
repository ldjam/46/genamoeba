import { System, Entity } from '../../ecs';
import { RemoveIfEmptyComponent } from '../components/RemoveIfEmptyComponent';
import { AmoebaPathfindingComponent } from '../components/AmoebaPathfindingComponent';
import { GridTargetComponent } from '../components/GridTargetComponent';
import { CellObjectComponent, AmoebaMovementComponent, AmoebaComponent, CellVelocityComponent } from '../components';
import { Builder } from '../builder';
import { AStarFinder } from "astar-typescript";
import { DIRECTIONS, GridCell } from '../models';
import { canMoveToCell, getEmptyNeighbourCell } from '../utils';


type EntityQuery = "pathfinders";
export class AmoebaPathfindingSystem extends System<EntityQuery> {
    constructor(
        private builder: Builder
    )
    {
        super()
    }

    public query = { 
        pathfinders: [
            AmoebaComponent,
            AmoebaPathfindingComponent,
            AmoebaMovementComponent,
            GridTargetComponent,
            CellObjectComponent
        ] 
    };

    public run = (
        dt: number, 
        { pathfinders } : Record<EntityQuery, Entity[]>
    ) => 
    {
        pathfinders.forEach(entity => {
            const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
            const movement = entity.getComponent<AmoebaMovementComponent>(AmoebaMovementComponent)!;
            const pathfinding = entity.getComponent<AmoebaPathfindingComponent>(AmoebaPathfindingComponent)!;
            const gridTarget = entity.getComponent<GridTargetComponent>(GridTargetComponent)!;
            const cellInfo = entity.getComponent<CellObjectComponent>(CellObjectComponent)!;

            if(amoeba.isWaiting() || entity.getComponent(CellVelocityComponent) || !getEmptyNeighbourCell(entity, this.builder.getGridLookup(), this.builder.getStore()))
            {
                return;
            }

            const searchSize = pathfinding.radius;
            const currentCellLocation = cellInfo.cell!;

            const matrixOffset = {
                col: Math.max(currentCellLocation.col - searchSize/2, 0),
                row: Math.max(currentCellLocation.row - searchSize/2, 0)
            };

            const moveMatrix = this.buildMoveMatrix(
                searchSize,
                [matrixOffset.col, matrixOffset.row]
            );

            var startingPoint = [ 
                currentCellLocation.col - matrixOffset.col,
                currentCellLocation.row - matrixOffset.row
            ];

            var endingPoint = [
                gridTarget.cell.col - matrixOffset.col,
                gridTarget.cell.row - matrixOffset.row
            ]; 

            moveMatrix[startingPoint[1]][startingPoint[0]] = 0;
            moveMatrix[endingPoint[1]][endingPoint[0]] = 0;

            var finder = new AStarFinder({
                grid: {
                    matrix: moveMatrix
                },
                diagonalAllowed: false,
                includeEndNode: true,
                includeStartNode: false
            });

            var path = finder.findPath(
                { x: startingPoint[0], y: startingPoint[1] },
                { x: endingPoint[0], y: endingPoint[1] }
            );

            // var cellPath = path.reduce((cells, b) => {
            //     cells.push({
            //         col: b[0],
            //         row: b[1]
            //     });
            //     return cells;
            // }, [] as GridCell[]);

            if (!path.length)
            {
                movement.nextDirection = {
                    col: 0,
                    row: 0
                };
            }
            else
            {
                movement.nextDirection = {
                    col: path[0][0] < startingPoint[0] ? -1 : path[0][0] > startingPoint[0] ? 1 : 0,
                    row: path[0][1] < startingPoint[1] ? -1 : path[0][1] > startingPoint[1] ? 1 : 0
                };
            }
        });
    }

    private buildMoveMatrix = (
        searchSize: number,
        offset: [number, number]
    ): number[][] =>
    {
        const moveMatrix: number[][] = [];

        for(let iRow = 0; iRow < searchSize; iRow++)
        {
            for(let iCol = 0; iCol < searchSize; iCol++)
            {
                if (!moveMatrix[iRow])
                {
                    moveMatrix[iRow] = [];
                }

                const col = iCol + offset[0];
                const row = iRow + offset[1];

                if (canMoveToCell({ row, col }, this.builder.getGridLookup(), this.builder.getStore()))
                {
                    moveMatrix[iRow][iCol] = 0;
                }
                else
                {
                    moveMatrix[iRow][iCol] = 1;
                }
            }
        }

        return moveMatrix;
    }
}
