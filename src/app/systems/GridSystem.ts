import { System, Entity } from "../../ecs";
import { GridComponent } from "../components/GridComponent";
import { GridHoverComponent } from "../components/GridHoverComponent";
import { GridSelectComponent } from "../components/GridSelectComponent";
import { GridCell } from "../models";
import { RemoveIfEmptyComponent } from "../components/RemoveIfEmptyComponent";

type EntityQuery = "grids" | "hovers" | "selects";
export class GridSystem extends System<EntityQuery> {
  public query = { grids: [GridComponent], hovers: [GridHoverComponent], selects: [GridSelectComponent] };
  public run = (dt : number, { grids, hovers, selects }: Record<EntityQuery, Entity[]>) => {
    const gridComponents = grids.map(x => x.getComponent<GridComponent>(GridComponent)!);

    hovers.forEach(hover => {
      gridComponents.forEach(grid => {
        const { cell } = hover.getComponent<GridHoverComponent>(GridHoverComponent)!;

        this.hoverGrid(grid, cell)

        hover.removeComponents(GridHoverComponent).addComponents(RemoveIfEmptyComponent.instance);
      });
    });

    selects.forEach(select => {
      gridComponents.forEach(grid => {
        const { cell } = select.getComponent<GridSelectComponent>(GridSelectComponent)!;

        this.selectGrid(grid, cell);

        select.removeComponents(GridSelectComponent).addComponents(RemoveIfEmptyComponent.instance);
      });
    });
  }

  private selectGrid(grid : GridComponent, coord : GridCell | null) {
    grid.select(coord);
  }

  private hoverGrid(grid : GridComponent, coord : GridCell | null) {
    grid.hover(coord);
  }
}
