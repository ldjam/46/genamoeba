import { System, Entity } from '../../ecs';
import { AmoebaComponent, LifeComponent, CellVelocityComponent } from '../components';
import { PANIC_LIFE, PANIC_SPEED_ADJ, MID_LIFE, MID_SPEED_ADJ } from '../constants';

type EntityQuery = "entities";
export class AmoebaHungerSystem extends System<EntityQuery> {
  public query = { entities: [AmoebaComponent, LifeComponent, CellVelocityComponent] };
  public run = (dt: number, { entities }: Record<EntityQuery, Entity[]>) => {
    entities.forEach(entity => {
      const amoeba = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
      const life = entity.getComponent<LifeComponent>(LifeComponent)!;
      const cellVelocity = entity.getComponent<CellVelocityComponent>(CellVelocityComponent)!;

      amoeba.setHungerState(AmoebaComponent.getHungerState(life.life));
      cellVelocity.pxSpeed = amoeba.speed * amoeba.getSpeedAdjustment();
    });
  };
}
