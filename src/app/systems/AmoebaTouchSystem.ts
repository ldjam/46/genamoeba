import { System, Entity } from '../../ecs';
import { AmoebaComponent, AmoebaTouchComponent, CellResetComponent, CellVelocityComponent } from '../components';
import { AMOEBA_TOUCH_WAIT } from '../constants';

type EntityQuery = "entities";
export class AmoebaTouchSystem extends System<EntityQuery> {
  public query = { entities: [AmoebaComponent, AmoebaTouchComponent] };
  public run = (dt: number, { entities }: Record<EntityQuery, Entity[]>) => {
    entities.forEach(x => {
      const amoeba = x.getComponent<AmoebaComponent>(AmoebaComponent)!;
      // Let's not keep touching the amoeba ;)
      x.removeComponents(AmoebaTouchComponent);

      this.stopAndResetPosition(x);
      amoeba.startWaiting(AMOEBA_TOUCH_WAIT);
      amoeba.pet();
    });
  };

  private stopAndResetPosition(entity : Entity) {
    entity.removeComponents(CellVelocityComponent);
    entity.addComponents(CellResetComponent.instance);
  }
}