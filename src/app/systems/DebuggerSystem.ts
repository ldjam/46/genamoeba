import { System, Entity } from '../../ecs';
import { DebuggerComponent } from '../components';
import { Builder } from '../builder';

type EntityQuery = "debuggers";
export class DebuggerSystem extends System<EntityQuery> {
  constructor(private readonly builder : Builder) {
    super();
  }

  public query = { debuggers: [DebuggerComponent] };
  public run = (dt: number, { debuggers }: Record<EntityQuery, Entity[]>) => {
      debuggers.forEach(entity => {
        const { view } = entity.getComponent<DebuggerComponent>(DebuggerComponent)!;

        view.update(this.builder.getStore());
      });
  };
}