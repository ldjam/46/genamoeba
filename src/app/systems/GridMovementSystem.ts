import { System, Entity } from '../../ecs';
import { CellObjectComponent, CellTransformComponent, CellVelocityComponent, CellHitComponent } from '../components';
import { Builder } from '../builder';
import { toPixelPosition, toGridPosition } from '../views';
import { areEqualCoordinates, GridCell } from '../models';
import { canMoveToCell } from '../utils';

const getDistance = (a : { x : number, y : number }, b : { x : number, y : number }) => {
  return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
};

/**
 * Returns a normalized point from a --> b
 */
const getDirection = (a : { x : number, y : number }, b : { x : number, y : number }) => {
  if (a.x === b.x && a.y === b.y) {
    return { x: 0, y: 0 };
  }
  const diffX = b.x - a.x;
  const diffY = b.y - a.y;
  const mag = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));

  return {
    x: diffX / mag,
    y: diffY / mag,
  };
};

type EntityQuery = "entities";

export class GridMovementSystem extends System<EntityQuery> {
  constructor(private readonly builder : Builder) {
    super();
  }

  public query = { entities: [CellObjectComponent, CellTransformComponent, CellVelocityComponent] };
  public run = (dt : number, { entities } : Record<EntityQuery, Entity[]>) => {
    const lookup = this.builder.getGridLookup();
    const store = this.builder.getStore();

    entities.forEach(entity => {
      const { cellObj, transform, velocity } = this.getCellComponents(entity);
      const { cell } = cellObj;

      if (!cell) {
        return;
      }

      const { row: rowChange, col: colChange } = velocity.direction;
      const nextCell = {
        row: cell.row + rowChange,
        col: cell.col + colChange
      };

      // cell is taken by someone other than me
      const canMove = canMoveToCell(nextCell, lookup, store, entity.id);
      if (!canMove) {
        // Oops! We can't go here :|
        //   - Remove velocity component
        //   - Reset to original grid position
        entity.removeComponents(CellVelocityComponent);
        transform.setPosition(toPixelPosition(cell));
        return;
      }

      const isMoved = this.moveTo(nextCell, transform, velocity);

      // If we didn't move, we are done :)
      if (!isMoved) {
        entity.addComponents(new CellHitComponent(nextCell));
        entity.removeComponents(CellVelocityComponent);
        return;
      }

      const transformCell = this.getCellAtCenter(transform);

      // Now we test if we need to move grid cell ownership...
      if (areEqualCoordinates(transformCell, nextCell)) {
        lookup.set(entity.id, nextCell);
        velocity.direction = { row: 0, col: 0 };
      }
    });
  };

  private moveTo(cell : GridCell, transform : CellTransformComponent, velocity : CellVelocityComponent) {
    const pos = transform.getPosition();
    const nextCellPos = toPixelPosition(cell);
    const direction = getDirection(pos, nextCellPos);

    const nextCellDistance = getDistance(pos, nextCellPos);
    const clippedSpeed = Math.min(velocity.pxSpeed, nextCellDistance);

    transform.setPosition({
      x: pos.x + direction.x * clippedSpeed,
      y: pos.y + direction.y * clippedSpeed,
    });

    return clippedSpeed !== 0;
  }

  private getCellAtCenter(transform : CellTransformComponent) {
    const center = this.getTransformCenter(transform);

    return toGridPosition(center.x, center.y);
  }

  private getTransformCenter(transform : CellTransformComponent) {
    const pos = transform.getPosition();
    const size = transform.getSize();
    return {
      x: pos.x + size.width / 2,
      y: pos.y + size.height / 2,
    };
  }

  private getCellComponents(entity : Entity) {
    return {
      cellObj: entity.getComponent<CellObjectComponent>(CellObjectComponent)!,
      transform: entity.getComponent<CellTransformComponent>(CellTransformComponent)!,
      velocity: entity.getComponent<CellVelocityComponent>(CellVelocityComponent)!,
    };
  }
}