import { System, Entity } from '../../ecs';
import { CellObjectComponent, CellTransformComponent, CellResetComponent } from '../components';
import { toPixelPosition } from '../views';

type EntityQuery = "entities";
export class CellResetSystem extends System<EntityQuery> {
  public query = { entities: [CellObjectComponent, CellTransformComponent, CellResetComponent] };
  public run = (dt: number, { entities }: Record<EntityQuery, Entity[]>) => {
    entities.forEach(x => {
      const { cell } = x.getComponent<CellObjectComponent>(CellObjectComponent)!;
      const transform = x.getComponent<CellTransformComponent>(CellTransformComponent)!;

      x.removeComponents(CellResetComponent);

      if (!cell) {
        return;
      }

      const pos = toPixelPosition(cell);
      transform.setPosition(pos);
    });
  };
};