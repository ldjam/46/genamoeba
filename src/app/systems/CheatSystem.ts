import { Entity, System } from "../../ecs";
import { Builder } from "../builder";
import { AmoebaComponent, AmoebaTraitsComponent, CellObjectComponent, LifeComponent } from "../components";
import { AMOEBA_REPRODUCTION_MAX_COUNT, MAX_LIFE } from "../constants";
import { GENE_TYPES, GENES } from "../models";

type EntityQuery = "amoebas";

export class CheatSystem extends System<EntityQuery> {
  constructor(private builder : Builder) {
    super();
  }

  public query = { amoebas: [AmoebaComponent, LifeComponent] };
  public run = (dt : number, { amoebas } : Record<EntityQuery, Entity[]>) => {
    Object.assign(window, {
      feed() {
        amoebas.forEach(entity => {
          const lifeComponent = entity.getComponent<LifeComponent>(LifeComponent)!;
          lifeComponent.life = MAX_LIFE;
        });
      },

      breed() {
        amoebas.forEach(entity => {
          const amoebaComponent = entity.getComponent<AmoebaComponent>(AmoebaComponent)!;
          amoebaComponent.pingReproduction(AMOEBA_REPRODUCTION_MAX_COUNT);
        });
      },

      seed() {
        for (let i = 0; i < Math.min(GENES.length, amoebas.length); i++) {
          const amoebaComponent = amoebas[i].getComponent<AmoebaComponent>(AmoebaComponent)!;
          amoebaComponent.carriedGene = GENES[i];
        }
      }
    });
  };
}