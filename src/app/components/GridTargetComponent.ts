import { Component } from '../../ecs';
import { GridCell } from '../models';

export class GridTargetComponent extends Component {
    constructor(
        public cell: GridCell
    )
    {
        super();
    }
}
