import { Component } from '../../ecs';
import { GeneType } from "../models";
import {
  AMOEBA_WAIT_MIN,
  AMOEBA_WAIT_SPREAD,
  AMOEBA_REPRODUCTION_WAIT_MIN,
  AMOEBA_REPRODUCTION_WAIT_SPREAD,
  AMOEBA_REPRODUCTION_MAX_COUNT,
  MID_WAIT_ADJ,
  PANIC_WAIT_ADJ,
  PANIC_LIFE,
  MID_LIFE,
  MID_SPEED_ADJ,
  PANIC_SPEED_ADJ,
} from '../constants';

export interface IAmoebaModel {
  pet(): void;

  pickUpGene(geneType : GeneType): void;
}

export type HungerStateKey = "happy" | "hungry" | "panic";
interface HungerState {
  readonly waitAdjustment : number,
  readonly speedAdjustment : number,
};
const HUNGER_STATES = {
  happy: {
    waitAdjustment: 1,
    speedAdjustment: 1,
  },
  hungry: {
    waitAdjustment: MID_WAIT_ADJ,
    speedAdjustment: MID_SPEED_ADJ,
  },
  panic: {
    waitAdjustment: PANIC_WAIT_ADJ,
    speedAdjustment: PANIC_SPEED_ADJ,
  },
} as Record<HungerStateKey, HungerState>;

export class AmoebaComponent extends Component {
  static getHungerState(life: number): HungerStateKey {
    return life < PANIC_LIFE ? "panic" : life < MID_LIFE ? "hungry" : "happy";
  }

  private wait : number = 0;
  private _reproductionCount : number = 0;
  private _reproductionWait : number = 0;
  private _hungerState : HungerState = HUNGER_STATES.happy;
  public carriedGene : GeneType | null = null;

  constructor(
    // I realize now that when I've said model... I really meant View :|
    private readonly view : IAmoebaModel,
    public readonly speed : number,
    public readonly dna : GeneType[]
  ) {
    super();
  }

  get reproductionCount() {
    return this._reproductionCount;
  }

  setHungerState(key: HungerStateKey) {
    this._hungerState = HUNGER_STATES[key];
  }

  canReproduce() {
    return this._reproductionCount > 0;
  }

  startWaiting(extra : number = 0) {
    this.wait = AMOEBA_WAIT_MIN + Math.random() * AMOEBA_WAIT_SPREAD + extra;
  }

  decreaseWait(x: number) {
    this.wait -= x * this._hungerState.waitAdjustment;
  }

  getSpeedAdjustment() {
    return this._hungerState.speedAdjustment;
  }

  isWaiting() {
    return this.wait > 0;
  }

  /**
   * This is a convenient function that updates the reproduction count and starts waiting
   * @param count 
   */
  pingReproduction(count?: number) {
    if (count) {
      this._reproductionCount = Math.min(AMOEBA_REPRODUCTION_MAX_COUNT, this.reproductionCount + count);
    }
    this.startReproductionWaiting();
  }

  private startReproductionWaiting() {
    this._reproductionWait = AMOEBA_REPRODUCTION_WAIT_MIN + Math.random() * AMOEBA_REPRODUCTION_WAIT_SPREAD;
  }

  decreaseReproductionWait(x: number) {
    this._reproductionWait -= x;
  }

  isReproductionWaiting() {
    return this._reproductionWait > 0;
  }

  pet() {
    this.view.pet();
    this.pingReproduction(1);
  }

  pickUpGene(geneType : GeneType) {
    this.carriedGene = geneType;
    this.view.pickUpGene(geneType);
    this.pingReproduction(2);
  }

  dropGene() {
    this.carriedGene = null;
    this.view.pickUpGene(null);
  }
}
