import { Component, EntityId } from "../../ecs";
import { GridLookup } from "../models";

/**
 * This component represents something that's occupying a cell
 */
export class CellObjectComponent extends Component {
  constructor(
    private readonly entityId : EntityId,
    private readonly lookup : GridLookup,
  ) {
    super();
  }

  get cell() {
    return this.lookup.getCells(this.entityId)[0];
  }

  dispose = () => {
    this.lookup.deleteEntityId(this.entityId);
  };
}
