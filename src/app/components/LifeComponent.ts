import { MAX_LIFE } from '../constants';
import { Component } from '../../ecs';

export interface ILifeView {
  updateLife(life: number): void;
}

/**
 * This component houses the "life" of an entity.
 * 
 * When paired with the "LifeSystem" it will delete the entity
 * if this value runs out.
 */
export class LifeComponent extends Component {
  private _life : number = MAX_LIFE;

  constructor(
    private _decay : number = 0,
    private readonly _view? : ILifeView,
  ) {
    super();
  }
  
  get life() {
    return this._life;
  }

  set life(val: number) {
    this._life = Math.min(val, MAX_LIFE);
    this._view?.updateLife(this._life);
  }

  get decay() {
    return this._decay;
  }

  set decay(val: number) {
    this._decay = val;
  }

  isAlive() {
    return this._life > 0;
  }
}
