import { Component } from '../../ecs';

/**
 * This component represents a touch action on the amoeba
 */
export class AmoebaTouchComponent extends Component {
  static readonly instance = new AmoebaTouchComponent();

  constructor() {
    super();
  }  
}
