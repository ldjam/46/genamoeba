import { Component } from "../../ecs";

export interface IMenuComponent {
  readonly isOpen : { get() : boolean };
  readonly open : () => void;
  readonly close : () => void;
  readonly dispose : () => void;
}

export class MenuComponent extends Component implements IMenuComponent {
  constructor(
    private source : IMenuComponent
  ) {
    super();
  }

  get isOpen() {
    return this.source.isOpen;
  }

  open = () => {
    this.source.open();
  };

  close = () => {
    this.source.close();
  };

  dispose = () => {
    this.source.dispose();
  };
}
