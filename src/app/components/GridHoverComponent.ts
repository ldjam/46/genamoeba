import { Component } from "../../ecs";
import { GridCell } from "../models";

/**
 * Component represents a request to hover over a specific grid cell
 */
export class GridHoverComponent extends Component {
  constructor(
    public cell : GridCell | null = null,
  ) {
    super();
  }
}