import { Component } from '../../ecs';

/**
 * This component tells the system that a cell object should
 * reset it's position to the cell it's on
 */
export class CellResetComponent extends Component {
  static readonly instance = new CellResetComponent();

  constructor() {
    super();
  }  
}
