import { Component } from "../../ecs";
import { GridCell } from "../models";

/**
 * Component represents a request to select a specific grid cell
 */
export class GridSelectComponent extends Component {
  constructor(
    public cell : GridCell | null = null,
  ) {
    super();
  }
}