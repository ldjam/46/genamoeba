import { Component, Entity } from '../../ecs';
import { FOOD_LIFE_BOOST } from '../constants';
import { AmoebaComponent } from "./AmoebaComponent";
import { LifeComponent } from "./LifeComponent";

export class FoodComponent extends Component {
  constructor(
    public readonly lifeBoost : number = FOOD_LIFE_BOOST,
  ) {
    super();
  }

  provideNutritionsTo(entity : Entity) {
    const lifeComponent = entity?.getComponent<LifeComponent>(LifeComponent);
    if (lifeComponent) {
      lifeComponent.life += this.lifeBoost;
    }

    const amoebaComponent = entity?.getComponent<AmoebaComponent>(AmoebaComponent);
    if (amoebaComponent && amoebaComponent.dna.length < 1) {
      amoebaComponent.pingReproduction(1);
    }
  }
}