import { Component } from '../../ecs';
import { IDebuggerView } from '../views';

export class DebuggerComponent extends Component {
  constructor(public readonly view: IDebuggerView) {
    super();
  }
}
