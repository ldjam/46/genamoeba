import { Component } from "../../ecs";
import { GridCell } from '../models';

/**
 * This component means that a cell was hit.
 * 
 * The system should look at the lookup and resolve what happens...
 */
export class CellHitComponent extends Component {
  constructor(
    public readonly cell : GridCell
  ) {
    super();
  }
}