import { Component } from '../../ecs';

type DirectionValue = -1 | 0 | 1;
type Direction = {
    col: DirectionValue
    row: DirectionValue
};

export class AmoebaMovementComponent extends Component {
  constructor(
      public nextDirection: Direction = { col: 0, row: 0}
  ) 
  {
    super();
  }
}
