import { Component, Entity } from '../../ecs';

/**
 * This component houses information like "is this cell object a rigid body or not"
 */
export class CellObjectPhysicsComponent extends Component {
  static defaultInstance = new CellObjectPhysicsComponent();
  static getOrDefault = (entity : Entity | null) => entity?.getComponent<CellObjectPhysicsComponent>(CellObjectPhysicsComponent) || CellObjectPhysicsComponent.defaultInstance;

  constructor(
    public readonly isRigid : boolean = true,
  ) {
    super();
  }
};