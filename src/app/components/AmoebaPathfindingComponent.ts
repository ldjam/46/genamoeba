import { Component } from '../../ecs';
import { GridCell } from '../models';

export class AmoebaPathfindingComponent extends Component {
  constructor(
      public radius: number = 30,
      public path: GridCell[] = []
  ) 
  {
    super();
  }
}
