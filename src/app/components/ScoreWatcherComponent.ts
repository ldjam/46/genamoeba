import { Component } from "../../ecs";
import { IScoreWatcher } from "../models/ScoreWatcher";

export class ScoreWatcherComponent extends Component {
  constructor(
    private readonly watcher: IScoreWatcher
  ) {
    super();
  }

  updateScore(score: number, collectedDna: Set<string>) {
    this.watcher.updateScore(score, collectedDna);
  }

  dispose = () => {
    this.watcher.dispose?.();
  }
}