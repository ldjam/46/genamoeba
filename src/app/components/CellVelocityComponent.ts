import { Component } from '../../ecs';
import { GridCell } from '../models';

export class CellVelocityComponent extends Component {
  constructor(
    public direction : GridCell,
    public pxSpeed : number
  ) {
    super();
  }  
}