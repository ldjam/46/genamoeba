import { Component } from '../../ecs';
import { GeneType } from '../models';

export class GeneComponent extends Component {
  constructor(
    public geneType : GeneType,
  ) {
    super();
  }
}
