import { Component } from '../../ecs';

export interface ITransformComponent {
  getPosition(): { x: number, y: number };
  setPosition(pos : { x: number, y: number }): void;
  getSize(): { width: number, height: number };
  setSize(size : { width: number, height: number }): void;
  dispose(): void;
}
/**
 * This component represents something that's occupying a cell
 */
export class CellTransformComponent extends Component implements ITransformComponent {
  constructor(
    private readonly source : ITransformComponent,
  ) {
    super();
  }

  getPosition() {
    return this.source.getPosition();
  }

  setPosition(pos : { x: number, y : number}) {
    this.source.setPosition(pos);
  }

  getSize() {
    return this.source.getSize();
  }

  setSize(size : { width: number, height: number }) {
    this.source.setSize(size);
  }

  dispose = () => {
    this.source.dispose();
  }
}