import { Component } from "../../ecs";
import { GridCell } from "../models";

export interface ICellMenuComponent {
  readonly cell : GridCell;
  readonly dispose : () => void;
}

export class CellMenuComponent extends Component implements ICellMenuComponent {
  constructor(
    private source : ICellMenuComponent
  ) {
    super();
  }
  
  public dispose = () => this.source.dispose();

  get cell() {
    return this.source.cell;
  }
}
