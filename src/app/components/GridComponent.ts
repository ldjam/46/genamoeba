import { Component } from "../../ecs";
import { GridCell } from "../models";

export interface IGridComponent {
  readonly selection : GridCell | null;
  readonly hovered : GridCell | null;
  select(co : GridCell | null): void;
  hover(co : GridCell | null): void;
  dispose(): void;
}

/**
 * `extends Component` is needed to query for the Component
 * 
 * So we can have some side-effect binding, this Component is an
 * adapter for the interface (which is implemented by `createGrid`)
 */
export class GridComponent extends Component implements IGridComponent {
  constructor(
    private source : IGridComponent
  )
  {
    super();
  }

  get selection() {
    return this.source.selection;
  }

  get hovered() {
    return this.source.hovered;
  }

  dispose = () => this.source.dispose();

  select(co : GridCell | null) {
    this.source.select(co);
  }

  hover(co : GridCell | null) {
    this.source.hover(co);
  }
}