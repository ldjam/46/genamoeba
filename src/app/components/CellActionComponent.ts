import { Component } from '../../ecs';
import { CellActionKey, GridCell } from '../models';

/**
 * This is a component that requests to the systems that
 * an action on a cell would like to be performed.
 */
export class CellActionComponent extends Component {
  constructor(
    public cell : GridCell,
    public action : CellActionKey,
    public payload? : any,
  ) {
    super();
  }
}