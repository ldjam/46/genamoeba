import { Component } from '../../ecs';

export class AmoebaTraitsComponent extends Component {
  constructor(
      public observableRange: number = 10,
      public aggressionScore: number = 1,
      public timidityScore: number = 1,
      public hungerScore: number = 3,
      public geneScore: number = 3,
      public moveCostFactor: number = 0.3
  ) 
  {
    super();
  }
}
