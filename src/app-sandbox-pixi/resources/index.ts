import * as PIXI from "pixi.js";
import { Resource } from '../../ecs';

class PixiSpriteResource implements Resource<PIXI.Sprite> {
  constructor(public value: PIXI.Sprite){}

  dispose(): void {
    this.value.destroy();
  }
}

export const createPixiSprite = (container: PIXI.Container) => {
  const sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
  container.addChild(sprite);
  return new PixiSpriteResource(sprite);
}
