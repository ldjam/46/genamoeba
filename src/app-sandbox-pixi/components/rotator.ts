import {Component} from "../../ecs";

export class RotatorComponent extends Component
{
    constructor(
        public speed: number
    )
    {
        super();
    }
}
