﻿import {Component} from "../../ecs";

export class TransformComponent extends Component
{
    constructor(
        public position: [number, number, number] = [0, 0, 0],
        public scale: [number, number] = [1, 1],
        public rotation: number = 0
    )
    {
        super();
    }
}
