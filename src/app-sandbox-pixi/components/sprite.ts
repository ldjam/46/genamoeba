﻿import {Component} from "../../ecs";

export class SpriteComponent extends Component
{
    constructor(
        public dimensions: [number, number], 
        public anchor: [number, number] = [0.5, 0.5],
        public tint: number = 0xFFFFFF
    )
    {
        super();
    }
}
