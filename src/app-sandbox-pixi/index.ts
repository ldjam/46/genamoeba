﻿import * as PIXI from "pixi.js";
import {ECS, EntityStore} from "../ecs";
import {PixiSpriteSyncSystem} from "./systems/PixiSpriteSyncSystem";
import {TransformComponent} from "./components/transform";
import {SpriteComponent} from "./components/sprite";
import { EntityRotationDestoryerSystem } from "./systems/EntityRotationDetroyerSystem";
import { RotatorComponent } from "./components/rotator";
import { EntityRotationSystem } from "./systems/EntityRotationSystem";

function main(mainEl: Element)
{
    const app = new PIXI.Application({
        width: 800, height: 600, backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1,
    });
    mainEl.appendChild(app.view);

    var entityStore = new EntityStore();

    for(let i = 0; i < 10; i++)
    {
        entityStore
            .createEntity()
            .addComponents(
                new TransformComponent(
                    [app.screen.width/2,app.screen.height/2,0],
                    [1, 1],
                    0),
                new SpriteComponent(
                    [getRandomNumber(0, 800), getRandomNumber(0, 600)],
                    [0.5, 0.5],
                    getRandomNumber(0, 16**6)
                ),
                new RotatorComponent(0.01)
            );
    }
    
    var logic = new ECS(entityStore);
    var rendering = new ECS(entityStore);

    logic
        .addSystem(new EntityRotationDestoryerSystem(2))
        .addSystem(new EntityRotationSystem());

    rendering
        .addSystem(new PixiSpriteSyncSystem(app.stage));

    app.ticker.add((delta) => {
        logic.updateSystems(delta);
        rendering.updateSystems(delta);
    });
}

function getRandomNumber(
    minNum: number,
    maxNum: number
)
{
    return minNum + Math.floor(Math.random() * (maxNum - minNum));
}

document.addEventListener('DOMContentLoaded', () => {
    const el = document.getElementById('main');

    if (!el) {
        throw new Error('main element not found');
    }

    main(el);
});
