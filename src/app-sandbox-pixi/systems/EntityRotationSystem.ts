import { System, Entity } from "../../ecs";
import { TransformComponent } from "../components/transform";
import { DisposedComponent } from "../components/disposed";
import { RotatorComponent } from "../components/rotator";

type EntityQuery = "entities";
export class EntityRotationSystem implements System<EntityQuery>
{
    constructor()
    {}

    public query = { entities: [ TransformComponent, RotatorComponent ] };
    public run = (dt: number, { entities }: Record<EntityQuery, Entity[]>) => 
    {
        entities.forEach(entity => {
            let transform = entity.getComponent<TransformComponent>(TransformComponent);
            let rotator = entity.getComponent<RotatorComponent>(RotatorComponent);

            if (!transform || !rotator)
            {
                return;
            }

            transform.rotation += rotator.speed * dt;
        });
    };
}
