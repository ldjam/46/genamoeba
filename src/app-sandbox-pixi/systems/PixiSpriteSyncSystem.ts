﻿import * as PIXI from "pixi.js";
import { System, Component, Entity } from "../../ecs";
import { SpriteComponent } from "../components/sprite";
import { TransformComponent } from "../components/transform";

const resourceKey: string = "pixi_sprite";

type EntityQuery = "entities";
export class PixiSpriteSyncSystem extends System<EntityQuery>
{    
    constructor(
        private container: PIXI.Container
    ) 
    {
        super();
    }
    
    public query = { entities: [ SpriteComponent, TransformComponent ] };
    public run = (
        dt: number, 
        { entities }: Record<EntityQuery, Entity[]>
    ) =>
    {
        entities.forEach(entity => {
            const spriteInfo = entity.getComponent<SpriteComponent>(SpriteComponent);
            const transform = entity.getComponent<TransformComponent>(TransformComponent);
            
            if (!spriteInfo || !transform)
            {
                throw new Error("Something went wrong");
            }

            const sprite = this.ensurePixiSprite(entity);
            sprite.width = spriteInfo.dimensions[0];
            sprite.height = spriteInfo.dimensions[1];
            sprite.anchor.set(spriteInfo.anchor[0], spriteInfo.anchor[1]);
            sprite.tint = spriteInfo.tint;
            sprite.position = new PIXI.Point(transform.position[0], transform.position[1]);
            sprite.rotation = transform.rotation;
        });
    }

    private ensurePixiSprite = (
        entity: Entity
    ): PIXI.Sprite =>
    {
        var sprite = 
            entity.getResource<PIXI.Sprite>(resourceKey);

        if (!sprite)
        {
            sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
            this.container.addChild(sprite);
            entity.addResource<PIXI.Sprite>({
                value: sprite,
                dispose: () => sprite?.destroy()
            }, resourceKey);
        }

        return sprite;
    }
}
