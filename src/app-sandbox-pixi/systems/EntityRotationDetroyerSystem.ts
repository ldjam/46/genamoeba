import { System, Entity } from "../../ecs";
import { TransformComponent } from "../components/transform";

type EntityQuery = "entities";
export class EntityRotationDestoryerSystem implements System<EntityQuery>
{
    constructor(
        private maxRotations: number
    )
    {}

    public query = { entities: [ TransformComponent ] };
    public run = (dt: number, { entities }: Record<EntityQuery, Entity[]>) => 
    {
        entities.forEach(entity => {
            let transform = entity.getComponent<TransformComponent>(TransformComponent);
            if (!transform)
            {
                return;
            }

            const numRotations = transform.rotation / (Math.PI * 2);
            if (numRotations > this.maxRotations)
            {
                entity.destroy();
            }
        });
    };
}
