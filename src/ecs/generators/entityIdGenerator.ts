﻿import { EntityId } from "../model";

export default function* entityIdGenerator(): IterableIterator<EntityId>
{
    let id = 0;

    while (true)
    {
        ++id;
        yield id;
    }
}
