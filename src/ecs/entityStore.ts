import { ComponentMap } from "./componentMap";
import { Component } from "./component";
import { EntityApi } from "./entityApi";
import entityIdGenerator from "./generators/entityIdGenerator";
import { EntityId, Entity, ComponentConstructor, Resource } from "./model";

export class EntityStore {
    private resources: Map<EntityId, Map<string, Resource<any>>> = new Map();
    private entities: Map<EntityId, ComponentMap> = new Map();
    private deletedEntities: Set<EntityId> = new Set();
    private entitiesByComponent: Map<ComponentConstructor, Set<EntityId>> = new Map();

    constructor(
        private readonly idGenerator = entityIdGenerator()
    )
    {}

    public createEntity(): Entity
    {
        const entity = this._createEntity();
        return new EntityApi(this, entity);
    }

    public findEntity(
        id: EntityId
    ): Entity | null
    {
        if (!this.entities.has(id))
        {
            return null;
        }

        return new EntityApi(this, id);
    }

    public _createEntity(): EntityId
    {
        const entityId = this.idGenerator.next().value;
        const componentMap = new ComponentMap();
        this.entities.set(entityId, componentMap);

        return entityId;
    }

    public query(
        ...componentCtors: ComponentConstructor[]
    ): Entity[]
    {
        const queryResults: Entity[] = [];

        if (componentCtors.length === 0)
        {
            return queryResults;
        }

        const componentSets =
            componentCtors.map(ctor =>
            {
                if (!this.entitiesByComponent.has(ctor))
                {
                    return new Set<number>();
                }

                return this.entitiesByComponent.get(ctor)!;
            });

        const smallestComponentSet =
            componentSets.reduce((smallest, set) =>
            {
                if (smallest == null)
                {
                    smallest = set;
                } else if (set.size < smallest.size)
                {
                    smallest = set;
                }

                return smallest;
            });

        const otherComponentSets =
            componentSets.filter(
                set => set !== smallestComponentSet,
            );

        smallestComponentSet.forEach(entityId => {
            const hasAll = otherComponentSets.every(set => set.has(entityId));

            if (hasAll)
            {
                queryResults.push(
                    new EntityApi(this, entityId)
                );
            }
        });

        return queryResults;
    }

    public deleteEntity(
        entityId: EntityId
    ): boolean
    {
        if (this.deletedEntities.has(entityId))
        {
            return false;
        }

        const hasDeleted = [this.deleteEntityComponents(entityId), this.deleteEntityResources(entityId)].reduce((a, b) => a || b);

        if (hasDeleted) {
            this.deletedEntities.add(entityId);
        }

        return hasDeleted;
    }

    public removeEntityComponents(
        entity: EntityId,
        ...componentCtors: ComponentConstructor[]
    ): EntityStore
    {
        if (this.deletedEntities.has(entity))
        {
            throw new Error('Entity has been deleted');
        }

        const entityComponents = this.entities.get(entity);

        if (entityComponents != null)
        {
            entityComponents.remove(
                ...componentCtors
            );

            componentCtors.forEach(ctor =>
            {
                if (this.entitiesByComponent.has(ctor))
                {
                    this.entitiesByComponent.get(ctor)!.delete(entity);
                }
            });
        }

        return this;
    }

    public addEntityComponents(
        entityId: EntityId,
        ...components: Component[]
    ): EntityStore
    {
        if (this.deletedEntities.has(entityId))
        {
            throw new Error("Entity has been deleted");
        }

        const entityComponents = this.getOrCreateComponentMapForEntity(entityId);
        entityComponents.set(...components);
        Array.from(entityComponents.keys()).forEach(componentCtor => {
            if (this.entitiesByComponent.has(componentCtor))
            {
                this.entitiesByComponent.get(componentCtor)!.add(entityId);
            } else
            {
                this.entitiesByComponent.set(componentCtor, new Set([entityId]));
            }
        })

        return this;
    }

    public getEntityComponents<T extends Component>(
        entityId: EntityId,
        ctor: ComponentConstructor
    ): T | undefined
    {
        const componentMap = this.entities.get(entityId);
        if (!componentMap)
        {
            throw new Error("missing component map");
        }

        return componentMap.get<T>(ctor);
    }

    public hasComponentAtEntity(entityId: EntityId, ctor: ComponentConstructor): boolean {
        const componentMap = this.entities.get(entityId);
        return Boolean(componentMap?.has(ctor));
    }

    public hasEntityComponents(entityId : EntityId)
    {
        return Boolean(this.entities.get(entityId)?.size());
    }

    public addResource<T>(entityId: EntityId, name: string, resource: Resource<T>) {
        const resources = this.getOrCreateResourcesForEntity(entityId);

        resources.set(name, resource);
    }

    public getResource<T>(entityId: EntityId, name: string): T | null {
        const entityResources = this.resources.get(entityId);

        if (!entityResources) {
            return null;
        }

        return entityResources.get(name)?.value || null;
    }

    private getOrCreateResourcesForEntity(entityId: EntityId): Map<string, Resource<any>> {
        const map = this.resources.get(entityId);

        if (!map) {
            const newMap = new Map<string, Resource<any>>();
            this.resources.set(entityId, newMap);
            return newMap;
        }

        return map;
    }

    private deleteEntityResources(
        entityId: EntityId
    ): boolean
    {
        if (!this.resources.has(entityId))
        {
            return false;
        }

        this.resources.get(entityId)?.forEach?.(x => {
            x.dispose();
        });

        this.resources.delete(entityId);

        return true;
    }

    private deleteEntityComponents(
        entityId: EntityId
    ): boolean
    {
        if (this.entities.has(entityId))
        {
            const componentMap = this.getOrCreateComponentMapForEntity(entityId);
            Array.from(componentMap.keys()).forEach(ctor => {
                this.entitiesByComponent.get(ctor)!.delete(entityId);
            });

            componentMap.clear();

            return true;
        }

        return false;
    }

    private getOrCreateComponentMapForEntity(
        entityId: EntityId
    )
    {
        let entityComponents = this.entities.get(entityId);
        if (!entityComponents)
        {
            entityComponents = new ComponentMap();
            this.entities.set(entityId, entityComponents);
        }

        return entityComponents;
    }

}
