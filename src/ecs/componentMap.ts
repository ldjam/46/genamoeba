import BitSet from "fast-bitset";
import { Component } from "./component";
import { ComponentConstructor } from "./model";

export class ComponentMap
{
    public bitmask = new BitSet(0);
    private readonly map = new Map<ComponentConstructor, Component>();

    public get<T extends Component>(
        ctor: ComponentConstructor
    ): T | undefined 
    {
        const component = 
            this.map.get(ctor);

        return component != null
            ? (component as T)
            : undefined;
    }

    public set(...components: Component[])
    {
        let mask = new BitSet(0);

        components.forEach(component => {
            const ctor = component.constructor as ComponentConstructor;

            if (!this.map.has(ctor))
            {
                mask = mask.or(ctor.bitmask);
            }

            this.map.set(ctor, component);
        });

        this.bitmask = this.bitmask.or(mask);
    }

    public remove(...componentCtors: ComponentConstructor[])
    {
        let mask = new BitSet(0);

        componentCtors.forEach(ctor => {
            if (this.map.has(ctor))
            {
                const component = this.map.get(ctor);
                component?.dispose();

                mask = mask.or(ctor.bitmask)
            }

            this.map.delete(ctor);
        });

        this.bitmask = this.bitmask.xor(mask);
    }

    public clear()
    {
        this.map.forEach(x => x.dispose());
        this.map.clear();
        this.bitmask.clear();
    }

    public keys()
    {
        return this.map.keys();
    }

    public has(componentCtor: ComponentConstructor)
    {
        return this.map.has(componentCtor);
    }

    public size() {
        return this.map.size
    }
}
