import { Component } from "./component";
import { ComponentConstructor, Entity } from "./model";

export abstract class System<T extends string>
{
    public abstract query: Record<T,ComponentConstructor[]>;
    public abstract run: (dt: number, entities: Record<T, Entity[]>) => void;
}
