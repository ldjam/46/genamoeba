import { System } from "./system";
import { EntityStore } from "./entityStore";

export class ECS {
    private _systems: System<any>[] = [];
    private _systemsToAdd: System<any>[] = [];
    private _systemsToRemove: System<any>[] = [];

    constructor(
        private readonly _entityStore: EntityStore
    )
    {}

    public addSystem(
        system: System<any>
    )
    {
        this._systemsToAdd.push(system);
        return this;
    }

    public removeSystem(
        system: System<any>
    )
    {
        this._systemsToRemove.push(system);
        return this;
    }

    public updateSystems(
        dt: number
    )
    {
        this.syncSystemsToRemove();
        this.syncSystemsToAdd();

        this._systems.forEach(
            system =>
            {
                const result = Object.entries(system.query).reduce((obj, [key, components]) => {
                    return Object.assign(obj, { [key]: this._entityStore.query(...components) });
                }, {});

                system.run(dt, result);
            }
        )
    }

    private syncSystemsToRemove()
    {
        if (this._systemsToRemove.length > 0)
        {
            this._systems =
                this._systems.filter(
                    system => this._systemsToRemove.includes(system));

            this._systemsToRemove = [];
        }
    }

    private syncSystemsToAdd()
    {
        if (this._systemsToAdd.length > 0)
        {
            this._systemsToAdd.forEach(
                system =>
                {
                    if (!this._systems.includes(system))
                    {
                        this._systems.push(system);
                    }
                }
            );

            this._systemsToAdd = [];
        }
    }
}
