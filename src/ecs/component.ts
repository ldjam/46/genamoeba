import BitSet from "fast-bitset";
import bitmaskGenerator from "./generators/bitmaskGenerator";

export class Component
{
    static _bitmask: BitSet | null = null;
    static _bitmaskGenerator = bitmaskGenerator();

    static get bitmask()
    {
        if (this._bitmask == null)
        {
            const nextBitMask = this._bitmaskGenerator.next().value;
            if (!nextBitMask)
            {
                throw new Error("error generating new bitmask");
            }

            this._bitmask = nextBitMask;
        }

        return this._bitmask;
    }

    public dispose = () => {};
}
