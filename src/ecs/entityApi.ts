import { Component } from "./component";
import { EntityStore } from "./entityStore";
import { EntityId, ComponentConstructor, Entity } from "./model";
import { Resource } from "./model/Resource";

export class EntityApi implements Entity {
    constructor(
        private readonly entityStore: EntityStore, 
        private readonly entityId: EntityId
    ){}

    public get id()
    {
        return this.entityId;
    }
    
    destroy() 
    {
        this.entityStore.deleteEntity(this.id);
    }

    addComponents(
        ...components: Component[]
    )
    {
        this.entityStore.addEntityComponents(this.id, ...components);
        return this;
    };

    removeComponents(
        ...ctors: ComponentConstructor[]
    )
    {
        this.entityStore.removeEntityComponents(this.id, ...ctors);
        return this;
    };

    getComponent<T extends Component>(
        ctor: ComponentConstructor
    ): T | undefined 
    {
        return this.entityStore.getEntityComponents(this.id, ctor);
    };

    addResource<T>(
        resource: Resource<T>,
        name: string = "default",
    ){
        this.entityStore.addResource(this.id, name, resource);
        return this;
    }

    getResource<T>(
        name: string = "default"
    ): T | null
    {
        return this.entityStore.getResource(this.id, name);
    }

    hasComponents()
    {
        return this.entityStore.hasEntityComponents(this.id);
    }
}
