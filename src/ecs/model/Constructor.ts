export type Constructor<T = unknown, Arguments extends any[] = any[]> = new (
    ...args: Arguments
) => T
