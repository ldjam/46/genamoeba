﻿import BitSet from "fast-bitset";
import { Component } from "../component";
import { Constructor } from "./Constructor";

export type ComponentConstructor = 
    Constructor<Component> & 
    {
        bitmask: BitSet
    }
