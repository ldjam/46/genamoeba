import { Disposable } from "./Disposable";

export interface Resource<T> extends Disposable {
  value: T  
}

export const asResource = <T extends Disposable>(obj : T) => {
  return {
    value: obj,
    dispose() {
      obj.dispose();
    }
  } as Resource<T>;
}
