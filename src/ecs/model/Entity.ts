import { Component } from "../component";
import { ComponentConstructor } from "./ComponentConstructor";
import { EntityId } from "./EntityId";
import { Resource } from "./Resource";

export interface Entity
{
    readonly id: EntityId;
    destroy: () => void;
    getComponent: <T extends Component>(ctor: ComponentConstructor) =>  T | undefined;
    addComponents: (...components: Component[]) => Entity;
    removeComponents: (...ctors: ComponentConstructor[]) => Entity;
    addResource: <T>(resource: Resource<T>, name?: string) => Entity;
    getResource: <T>(name?: string) => T | null;
    hasComponents: () => Boolean;
}
