function* resourceHandleGenerator()
{
    let n = 0;

    while(true)
    {
        ++n;
        yield n;
    }
}

export class ResourceCache<T>
{
    private readonly _cache = new Map<number, T>();
    private readonly _handleGenerator = resourceHandleGenerator()

    get = (handle: number) => 
    {
        return this._cache.get(handle);
    };

    add = (item: T) =>
    {
        const handle = this._handleGenerator.next().value;
        if (!handle)
        {
            return 0;
        }

        this._cache.set(handle, item);
        return handle;
    }

    remove = (handle: number) =>
    {
        this._cache.delete(handle);
    }

    clear = () =>
    {
        this._cache.clear();
    }
}
