declare module "fast-bitset"
{
    export default class BitSet 
    {
        constructor(n?: number);
        public get(idx: number): boolean;
        public set(idx: number): boolean;
        public setRange(from: number, to: number): boolean;
        public unset(idx: number): boolean;
        public unsetRange(from: number, to: number): boolean;
        public toggle(idx: number): boolean;
        public toggleRange (from: number, to: number): boolean;
        public clear(): boolean;
        public clone(): BitSet;
        public dehydrate(): string;public and(bs: BitSet): BitSet;
        public and(idx: number): BitSet;
        public or(bs: BitSet): BitSet;
        public or(idx: number): BitSet;
        public xor(bs: BitSet): BitSet;
        public xor(idx: number): BitSet;
        public isEmpty(): boolean;
        public isEqual(bs: BitSet): boolean;
        public toString(): string;
    }
}


